DoApplyCompantionMatrixTest := function(p,k,d)
  local c,f,g,i,m,q,t,tt,v,w,x,y,pos1,pos2,N;
  if IsOddInt(d) then 
      d := d + 1; 
      Print("Corrected d to ",d," to make it even!\n");
  fi;
  f := GF(p,k);
  q := p^k;
  v := ListWithIdenticalEntries(d,Zero(f));
  ConvertToVectorRep(v,q);
  Randomize(v);
  v := CVec(v);
  c := List([1..d],i->ShallowCopy(v));
  c := CMat(c,CVecClass(p,k,d));
  m := MutableCopyMat(c);
  for i in [1..d] do 
    repeat x := Random(f); until not(IsZero(x));
    MultRowVector(m[i],x);
  od;
  c := ZeroMutable(c);
  for i in [1,3..d-1] do
    c[i][i+1] := One(f);
    repeat x := Random(f); until not(IsZero(x));
    c[i+1][i] := x;
    repeat y := Random(f); until not(IsZero(y));
    c[i+1][i+1] := y;
  od;
  w := [x,y];
  ConvertToVectorRep(w,q);
  w := CVec(w);
  # Now c is a block-diagonal-companion matrix [[0,1],[x,y]],
  #     w is a vector [x,y]
  #     m is a (dense) d by d matrix
  # and v is a vector of length d

  Print("Calibrating number of repetitions (3 sec)...\c");
  t := Runtime() + 3000;
  N := 1;
  repeat
    for i in [1..d] do
        x := ApplyCompanionMatrix(v,w);
    od;
    N := N + 1;
  until Runtime() >= t;
  Print(" done!\n");
  Print("Doing ",N," repetitions.\n");

  Print("Testing ApplyCompanionMatrix on vectors...\n");
  tt := EmptyPlist(10);
  t := Runtime();
  for i in [1..d*N] do
    x := ApplyCompanionMatrix(v,w);
  od;
  Add(tt,Runtime() - t);

  #Print("Testing ApplyCompanionMatrix on matrices...\n");
  #t := Runtime();
  #for i in [1..N] do
  #  x := ApplyCompanionMatrix(m,w);
  #od;
  #Add(tt,(Runtime() - t)*d);

  Print("Testing multiplying a vector by a companion-mat ",
        "from the right...\n");
  t := Runtime();
  for i in [1..20*d*N] do
    y := v*c;
  od;
  Add(tt,QuoInt((Runtime() - t),20));

  Print("Testing multiplying a matrix by a companion-mat ",
        "from the right...\n");
  t := Runtime();
  for i in [1..20*N] do
    x := m*c;
  od;
  Add(tt,QuoInt((Runtime() - t)*d,20));

  Print("Testing one ScalarProduct...\n");
  t := Runtime();
  for i in [1..100*d*N] do
    x := ScalarProduct(m[1],m[2]);
  od;
  Add(tt,QuoInt(Runtime() - t,100));

  Print("Testing one EntryOfMatrixProduct...\n");
  pos1 := Random(1,d);
  pos2 := Random(1,d);
  t := Runtime();
  for i in [1..40*d*N] do
    x := EntryOfMatrixProduct(m,m,pos1,pos2);
  od;
  Add(tt,QuoInt((Runtime() - t),40));

  Print("Testing multiplying a matrix by a companion-mat ",
        "from the left...\n");
  t := Runtime();
  for i in [1..20*N] do
    x := c*m;
  od;
  Add(tt,QuoInt((Runtime() - t)*d,20));

  Print("Testing TransposedMat...\n");
  t := Runtime();
  for i in [1..20*N] do
    x := TransposedMat(m);
  od;
  Add(tt,QuoInt((Runtime() - t)*d,20));

  Add(tt,2*d*tt[4]);

  Print("Done. All times are for single operations in nanoseconds.\n");
  g := function(t) return QuoInt(t*1000000 + (QuoInt(d*N,2)),d*N); end;
  return List(tt,g);
end; 
  
DoSomeApplyCompanionMatrixTests := function(d)
  local c,i,k,l,p,q,r,res;
  l := Filtered([2..256],IsPrimePowerInt);
  res := [];
  for q in l do
    Print("q=",q,":\n");
    c := Collected(Factors(q));
    p := c[1][1];
    k := c[1][2];
    r := DoApplyCompantionMatrixTest(p,k,d);
    Add(res,r);
  od;
  for i in [1..Length(l)] do
    q := l[i];
    Print("q=",q," ",res[i],"\n");
  od;
end;

