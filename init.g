#############################################################################
##
##  init.g                cond package
##                                                          Max Neunhoeffer
##                                                             Felix Noeske
##
##  Copyright 2006 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Reading the declaration part of the cond package.
##
#############################################################################

ReadPackage("cond","gap/cond.gd");
ReadPackage("cond","gap/homcond.gd");
