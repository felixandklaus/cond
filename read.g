#############################################################################
##
##  read.g                cond package
##                                                          Max Neunhoeffer
##                                                             Felix Noeske
##
##  Copyright 2006 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Reading the implementation part of the cond package.
##
#############################################################################

ReadPackage("cond","gap/cond.gi");
ReadPackage("cond","gap/homcond.gi");

Cond_Compiled_Module := Filename(DirectoriesPackagePrograms("cond"), "comp.so");
if Cond_Compiled_Module <> fail then
    LoadDynamicModule(Cond_Compiled_Module);
fi;
