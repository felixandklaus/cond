HomcondComp := 
function( M, N, info )
  local z0, o0, ShredMatrix, Mshred, newclass, T, pairs, sl, tl, dl, nl, 
        sll, tll, dll, nll, R, L, sum, mat, vec, l, ll, t, tt, s, ss, i, 
	r, bb, ii, jj, v, scaprod, scaprodsrows, entryofmatprod, Rorig;
  # M: matrix action of g on M TRANSPOSED
  # N: matrix action of g on module N
  # BOTH IN SSB! N.B. ssb for contragredient of M!
  # info: info record for Mstar x N
  z0 := Zero( BaseField( N ) );
  o0 := One( BaseField( N ) );
  scaprod := ApplicableMethod( ScalarProduct, [N[1],N[1]] );
  scaprodsrows := ApplicableMethod( ScalarProductsRows, [N,N,Length(N)] );
  entryofmatprod := ApplicableMethod( EntryOfMatrixProduct, [N,N,1,1] );

  ShredMatrix := function( mat, ranges )
    local shreds, di, dii, i, ii, k, kk;
    shreds := [];
    for i in [ 1 .. Length( ranges ) ] do
      shreds[ i ] := [];
      di := Length( ranges[ i ][ 1 ] );
      for ii in [ 1 .. Length( ranges ) ] do
        dii := Length( ranges[ ii ][ 1 ] );
	shreds[ i ][ ii ] := [];
	if di = 1 and dii = 1 then
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] :=
	           mat[ ranges[ i ][ k ][ 1 ] ][ ranges[ ii ][ kk ][ 1 ] ];
	    od;
	  od;
	else
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] := 
		   ExtractSubMatrix( mat, ranges[ i ][ k ],
					  ranges[ ii][ kk ] );
              if dii = 1 and info.SplittingFields[ i ] = 1 and
                             info.SplittingFields[ii] = 1 then
                  shreds[ i ][ ii ][ k ][ kk] := 
                    TransposedMat( shreds[ i ][ ii ][ k ][ kk] );
              fi;
	    od;
	  od;
	fi;
      od;
    od;
    return shreds;
  end;
  
  # Shred now to save two loop iterations
  Mshred := ShredMatrix( M, info.Mranges );
  newclass := CVecClass( N[ 1 ], info.Dim ); 
  T:= CVEC_ZeroMat( info.Dim, newclass );
  pairs := Length( info.Mranges );
  for l in [ 1 .. pairs ] do
    Print( "#I Doing ",l," of ", pairs,"        \r");
    sl := Length( info.Mranges[ l ] );
    tl := Length( info.Nranges[ l ] );
    dl := Length( info.Nranges[ l ][ 1 ] );
    nl := info.SplittingFields[ l ];
    for ll in [ 1 .. pairs ] do
      #Print( "#I Doing (",l,", ",ll,") of ", pairs,"^2        \r");
      sll := Length( info.Mranges[ ll ] );
      tll := Length( info.Nranges[ ll ] );
      dll := Length( info.Nranges[ ll ][ 1 ] );
      nll := info.SplittingFields[ ll ];
      ### catch the special case of scalars ###
      if dl = 1 and dll = 1 then
	for t in [ 1 .. tl ] do
	  for tt in [ 1 .. tll ] do
	    R := N[info.Nranges[ l ][ t ][ 1 ]]
		  [info.Nranges[ ll ][ tt ][ 1 ]];
            if not IsZero( R ) then
	      for s in [ 1 .. sl ] do
                v := T[info.Tranges[l][t][s][1]];
		for ss in [ 1 .. sll ] do
		  L := Mshred[ ll ][ l ][ ss ][ s ];
		  v[info.Tranges[ll][tt][ss][1]] := L * R; 
		od;
	      od;
	    fi;
	  od;
	od;
      else ################################################
	for t in [ 1 .. tl ] do
	  for tt in [ 1 .. tll ] do
	    R := ExtractSubMatrix( N, info.Nranges[ l ][ t ],
					   info.Nranges[ ll ][ tt ] );
            # we no longer transpose R:
	    #if not ( dl = 1 and nll = 1 and nl = 1 ) then
	    #  # turn column to row or transpose mat
	    #  R := TransposedMat( R );
	    #fi;
	    for s in [ 1 .. sl ] do
	      for ss in [ 1 .. sll ] do
		L := Mshred[ ll ][ l ][ ss ][ s ];
		if nll = 1 then 
                  if nl = 1 and dl = 1 then
                    # sum := ScalarProduct(TransposedMat(L)[1],R[1]); #
                    # is already done in ShredMatrix! #
                    sum := scaprod(L[1],R[1]);

                    T[ info.Tranges[ l ][ t ][ s ][ 1 ] ]
                     [ info.Tranges[ ll ][ tt ][ ss ][ 1 ] ] := 
                             sum * ( o0 * dll )^-1;
                  else # nl bel.
                    Rorig := R;
                    for i in [1..nl] do
                      sum := z0;
                      for r in [1 .. dll] do
                        #sum := sum + ScalarProduct( L[ r ], R[ r ] );
                        sum := sum + entryofmatprod( L, R, r, r );
                      od;
                      # was (when R was transposed!):
                      #sum := scaprodsrows( L, R, dll );
                      #
                      T[ info.Tranges[ l ][ t ][ s ][ i ] ]
                       [ info.Tranges[ ll ][ tt ][ ss ][ 1 ] ] := 
                               sum * ( o0 * dll )^-1;
                      if i < nl then
                        R := info.CompanionMatsBig[l] * R;
                        #L := ApplyCompanionMatrix( L, info.CoeffsMinPols[ l ]);
                      fi;
                    od;
                    R := Rorig;
                  fi;
		else # nll > 1
                  Rorig := R;
		  for i in [1..nl] do
		    mat := ZeroMatrix( nll, nll, L );
		    vec := ZeroMutable( mat[1] );
		    for bb in [ 0, nll .. dll - nll ] do
		      for ii in [ 1 .. nll ] do
		        for jj in [ 1 .. nll ] do
		          #vec[jj] := scaprod(L[bb+ii], R[bb+jj]);
		          vec[jj] := entryofmatprod(L,R,bb+ii,bb+jj);
		        od;
		        AddRowVector( mat[ii], vec );
		      od;
		    od;
		    MultRowVector( vec, z0 );
		    vec[ 1 ] := TraceMat( mat );
		    for ii in [2..nll] do
		      #mat := ApplyCompanionMatrix( mat,
		      # 	      info.CoeffsMinPols[ll] );
                      mat := mat * info.CompanionMatsSmall[ll];
		      vec[ii] := TraceMat( mat );
		    od;
		    CopySubVector( vec * info.Bs[ ll ], 
		       	    T[ info.Tranges[ l ][ t ][ s ][ i ] ],
		       	    [1..nll], info.Tranges[ ll ][ tt ][ ss ] );
		    if i < nl then
                      #L := ApplyCompanionMatrix( L, info.CoeffsMinPols[ l ]);
                      R := info.CompanionMatsBig[l] * R;
		    fi;
		  od;
                  R := Rorig;
		fi;
	      od;
	    od;
	  od;
	od;
      fi;
    od;
  od;
  Print("\n");
  return T;
end;


