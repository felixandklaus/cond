#############################################################################
##
##  homcond.gd              cond package
##                                                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2006 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Declaration stuff for condensation of homomorphism spaces.
##
#############################################################################

DeclareGlobalFunction( "PreTcond" );
DeclareGlobalFunction( "Tcond" );
DeclareGlobalFunction( "PreHomcond" );
DeclareGlobalFunction( "Homcond" );


