#############################################################################
##
##  cond.gd              cond package
##                                                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2006 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Declaration stuff for cond
##
#############################################################################

DeclareGlobalFunction( "IsBlockLowerTriangularMat" );
DeclareOperation( "ApplyCompanionMatrix", [IsRowVectorObj, IsRowVectorObj]);
DeclareOperation( "ApplyCompanionMatrix", [IsMatrixObj, IsRowVectorObj]);
DeclareGlobalFunction( "PermutationCond" );
# If the cvec package is already there and loaded, then this is
# already done:
if not(IsBound(ScalarProductsRows)) then
  DeclareOperation( "ScalarProductsRows", 
                    [ IsMatrixObj, IsMatrixObj, IsPosInt ] );
fi;


#DeclareGlobalFunction("PermutationCond2");
