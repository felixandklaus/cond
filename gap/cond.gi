#############################################################################
##
##  cond.gi           cond package
##                                                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2006 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Implementation stuff for cond
##
#############################################################################

InstallGlobalFunction( IsBlockLowerTriangularMat,
  function( m, acs, dims )
    local curdim,d,i,l,nextdim,s;
    if not(ForAll(acs,x->x >= 1 and x <= Length(dims))) then
        Error("Second argument acs must be a list of positions in third ",
              "argument dims");
        return;
    fi;
    if not(Sum(dims{acs})) = Length(m) and Length(m) = Length(m[1]) then
        Error("Matrix dimensions do not match block sizes");
        return;
    fi;
    curdim := 0;
    l := Length(m);
    for i in [1..Length(acs)-1] do
        d := dims[acs[i]];
        nextdim := curdim + d;
        s := ExtractSubMatrix( m, [curdim+1..curdim+d], [nextdim+1..l] );
        if not(IsZero(s)) then
            return false;
        fi;
        curdim := nextdim;
    od;
    return true;
  end );

InstallMethod( ApplyCompanionMatrix, "for a row vector and a row vector",
  [ IsRowVectorObj, IsRowVectorObj ],
  function( v, c )
    local cc,f,i,m,n,vv,vvv,z;
    m := Length(v);
    n := Length(c);
    f := BaseDomain(v);
    z := Zero(f);
    if m mod n <> 0 then
        Error("Length of vector must be a multiple of size of comp. matrix");
        return fail;
    fi;
    vv := ZeroMutable(v);
    CopySubVector(v,vv,[1..m-1],[2..m]);
    vvv := ZeroMutable(v);
    for i in [n+1,2*n+1..m-n+1] do vv[i] := z; od;
    cc := ShallowCopy(c);
    for i in [0,n..m-n] do
        CopySubVector(c,cc,[1..n],[1..n]);
        MultRowVector(cc,v[i+n]);
        CopySubVector(cc,vvv,[1..n],[1+i..n+i]);
    od;
    AddRowVector(vv,vvv);
    return vv;
  end );

InstallMethod( ApplyCompanionMatrix, 
  "for a row list matrix and a row vector",
  [ IsRowListMatrix, IsRowVectorObj ],
  function( m, c )
    local i,mm;
    mm := m{[]};
    for i in [1..Length(m)] do
        Add(mm,ApplyCompanionMatrix(m[i],c));
    od;
    return mm;
  end );

InstallGlobalFunction( PermutationCond, 
function( reps, noreps, elm, condgens, op, lookupfunc, lookupdata )
  #       reps: an iterator giving the representatives of the condensation
  #             subgroup's orbits
  #     noreps: #reps
  #       elm : element of algebra to be condensed
  #   condgens: list giving the generators of the condensation subgroup in
  #             proper basis!
  #         op: specifies the action
  # lookupfunc: function which determines the suborbit of a given point
  # lookupdata: data for the lookupfunc
  local mat, i, r, o, x, l, t;
  mat := List( [1..noreps],i->0*[1..noreps] );
  i := 1;
  while not IsDoneIterator( reps ) do
    r := NextIterator( reps );
    o := Orb( condgens, r , op, rec( report:=20000 ) );
    Enumerate( o );
    for t in [1..Length(o)] do
      x := op( o[t], elm );
      l := lookupfunc( lookupdata, x );
      mat[ i ][ l ] := mat[ i ][ l ] + 1;
    od;
    Print(i,"\r");
    i := i + 1;
  od;
  return mat;
end );

#PermutationCond2:= 
#function( nrorbs, elm, op,
#          sizehelpergrp, helpergens, helperreps, 
#          lookupfunc, lookupdata, suborbssetup )
#  local mat, norepshelper, oo, x, l, i, j, t;
#  #    suborbs: list of orbits of condenstion subgroup enumerated by
#  #             suborbits with respect to a helper subgroup
#  #       elm : element of algebra to be condensed
#  # helpergens: list of generators of helper subgroup (mind the basis!)
#  #         op: specifies the action
#  # lookupfunc: function which determines the orbit of a given point
#  #             in the list suborbs
#  # lookupdata: data for the lookupfunc
#  mat := List( [1..nrorbs],i->0*[1..nrorbs] );
#  for i in [1..nrorbs] do
#    Print("#I Current orbit: ",i," of ",String(nrorbs),".\n");
#    norepshelper := Length( helperreps[ i ] );
#    for j in [1..norepshelper] do
#      Print("Suborbit ",j," of ",norepshelper,"      \r");
#      oo := Orb( helpergens, helperreps[ i ][ j ], op, rec(
#	         grpsizebound:=sizehelpergrp ) );
#      Enumerate( oo );
#      for t in [1..Length(oo)] do
#        x := op( oo[t], elm );
#        l := lookupfunc( lookupdata, suborbssetup, x );
#        mat[ i ][ l ] := mat[ i ][ l ] + 1;
#      od;
#    od;
#  od;
#  return mat;
#end;

# This is also in the cvec package:
InstallMethod( ScalarProductsRows, "for two matrices, and a positive integer",
  [ IsMatrixObj, IsMatrixObj, IsPosInt ],
  function( m, n, l )
    local i,sum;
    sum := ScalarProduct( m[1], n[1] );
    for i in [2..l] do
        sum := sum + ScalarProduct( m[i], n[i] );
    od;
    return sum;
  end );

