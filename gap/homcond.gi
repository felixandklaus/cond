#############################################################################
##
##  homcond.gi              cond package
##                                                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2006 Lehrstuhl D für Mathematik, RWTH Aachen
##
##  Implementation stuff for condensation of homomorphism spaces.
##
#############################################################################

DoworkIdem := function( t, res, subres )
  
  if res = fail then
    return subres;
  else
    return res + subres * t;
  fi;
end;

PermutedSummands := function(re,plan)
  local newgens, basperm, pos, row, dim, i, k, j;
  # re a CHOP-record after SemiSimplicityBasis 
  # plan a list of length re.acs, containing the numbers of summands in
  # the order as they should appear in the resulting module.
  # Changes re in place, updates re.gensinssb.

  if Set(plan) <> [1..Length(re.acs)] then
      Error("Plan must describe a permutation!");
  fi;
  newgens := []; #FIXME Speicher!
  for i in [1..Length(re.gensinssb)] do
    Add( newgens, ZeroMutable( re.gensinssb[ 1 ] ) );
  od;
  
  basperm := [];   # Here we collect the basis permutation, such that in
                   # the end, we have to do:
                   #   basnew := bas{basperm}
  pos := 0;
  
  for i in plan do
    row := 0;
    for k in [1..i-1] do
      row := row + Dimension( re.db[ re.acs[ k ] ] );
    od;
    dim := Dimension( re.db[ re.acs[ i ] ] );
      for j in [1..Length(re.gensinssb)] do
	CopySubMatrix( re.gensinssb[j], newgens[j], 
	               [ row + 1 .. row + dim ],
	               [ pos + 1 .. pos + dim ],
	               [ row + 1 .. row + dim ],
	               [ pos + 1 .. pos + dim ] );
        basperm{ [ pos + 1 .. pos + dim ] } := [ row + 1 .. row + dim ];
      od;
      pos := pos + dim;
  od;
  re.gensinssb := newgens;
  re.basis := re.basis{basperm};
  for i in [1..Length(re.ibasis)] do
    re.ibasis[i] := re.ibasis[i]{basperm};
  od;
  return re;
end;

CondShuffle:=function(re)
  # Sorts output of direct sum chop, st basis is ssb
  local OurSorter, OurSorter2, plan, planCF;
  OurSorter := function(a,b)
    if Dimension( re.db[ re.acs[ a ] ] ) <
       Dimension( re.db[ re.acs[ b ] ] ) then
      return true;
    elif Dimension( re.db[ re.acs[ a ] ] ) >
         Dimension( re.db[ re.acs[ b ] ] ) then
      return false;
    else
      return re.acs[ a ] < re.acs[ b ];
    fi;
  end;
  plan := [ 1 .. Length( re.acs ) ];
  Sort( plan, OurSorter );
  PermutedSummands( re, plan );
  re.acs:=re.acs{ plan };
end;


InstallGlobalFunction( PreTcond,
function( Mstar, N )
# INPUT: chop record with ssb of CONTREGRADIENT of left factor (M)
#        chop record with ssb of right factor (N)
#        SHUFFLED! (for the beauty of it)
#        Both where chopped with the SAME database of irreds
  local ParseACS, isotypesMstar, isotypesN, isotypepairs, Mstarranges, 
        Nranges, Tranges, dimcount, dim, TKInfo, helper, i, l, k;
  
  ParseACS:=function( crec )
    # determines the ranges of occurrance of every isotype, ie the row
    # number ranges  of the basis which correspond to a particular isotype
    local ranger, currentisotype, dimcount, isotype, i;
    ranger := [ ];
    currentisotype := 0;
    dimcount := 0;
    for i in [ 1 .. Length( crec.acs ) ] do
      isotype := crec.acs[ i ];
      if not ( isotype = currentisotype ) then
	currentisotype := isotype;
	ranger[ currentisotype ] := [ ];
      fi;
      Add( ranger[ currentisotype ], 
      [ dimcount + 1 .. dimcount + Dimension( crec.db[ currentisotype ] ) ] );
      dimcount := dimcount + Dimension( crec.db[ currentisotype ] );
    od;
    return ranger;
  end;

  isotypesMstar := Filtered( [1..Length(Mstar.mult)],
		              i->Mstar.mult[i]>0 );
  isotypesN := Filtered( [1..Length(N.mult)], i->N.mult[i]>0 );
  isotypepairs := Intersection( isotypesMstar, isotypesN );
  #supplemental for backwards compatibility
  helper := List( isotypepairs, x->Position(N.acs,x) );
  SortParallel( helper, isotypepairs );
  
  #
  Mstarranges := ParseACS( Mstar ){ isotypepairs };
  Nranges := ParseACS( N ){ isotypepairs };
  
  
  
  Tranges := [];
  dimcount := 0;
  for i in [ 1 .. Length( isotypepairs ) ] do
    dim := DegreeOfSplittingField(Mstar.db[isotypepairs[i]]);
    Tranges[ i ] := [];
    for l in [ 1 .. N.mult[ isotypepairs[ i ] ] ] do
      Tranges[ i ][ l ] := [];
      for k in [ 1 .. Mstar.mult[ isotypepairs[ i ] ] ] do
	Tranges[ i ][ l ][ k ] := [dimcount+1..dimcount+dim];
	dimcount := dimcount + dim;
      od;
    od;
  od;
  TKInfo:=rec();
  # NEW for uncondensation
  TKInfo.Mdim:= Dimension( Mstar.module );
  TKInfo.Ndim:= Dimension( N.module );
  TKInfo.CFdims:= List( Mstar.db, Dimension );
  #####
  TKInfo.isotypepairs:=isotypepairs;
  #
  #TKInfo.CfIndexM:=isotypepairs;
  #TKInfo.CfIndexN:=isotypepairs;
  #TKInfo.NCf := Length( isotypepairs );
  #
  TKInfo.SplittingFields := List( Mstar.db{isotypepairs},
                                  DegreeOfSplittingField );
				  
  TKInfo.Mstarranges := Mstarranges;
  TKInfo.Nranges := Nranges;
  TKInfo.Tranges := Tranges;
  TKInfo.Dim := 0;
  for i in isotypepairs do
    TKInfo.Dim := TKInfo.Dim + Mstar.mult[ i ] * N.mult[ i ] * 
                  DegreeOfSplittingField( Mstar.db[ i ] );
  od;
  return TKInfo;
end );

InstallGlobalFunction( Tcond,
function( Mtr, N, info )
  local z0, ShredMatrix, Mtrshred, newclass, T, pairs, mi, ni, di, mii, nii, dii, Nblock, Mblock, sum, i, ii, l, ll, k, kk, r;
  # M: matrix action of g on M TRANSPOSED
  # N: matrix action of g on module N
  # BOTH IN SSB! N.B. ssb for contragredient of M!
  # info: info record for Mstar x N
  z0 := Zero( BaseField( N ) );
  ShredMatrix := function( mat, ranges )
    local shreds, di, dii, i, ii, k, kk;
    shreds := [];
    for i in [ 1 .. Length( ranges ) ] do
      shreds[ i ] := [];
      di := Length( ranges[ i ][ 1 ] );
      for ii in [ 1 .. Length( ranges ) ] do
        dii := Length( ranges[ ii ][ 1 ] );
	shreds[ i ][ ii ] := [];
	if di = 1 and dii = 1 then
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] :=
	           mat[ ranges[ i ][ k ][ 1 ] ][ ranges[ ii ][ kk ][ 1 ] ];
	    od;
	  od;
	else
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] := 
		   ExtractSubMatrix( mat, ranges[ i ][ k ],
					  ranges[ ii][ kk ] );
	      if dii = 1 then
	        shreds[ i ][ ii ][ k ][ kk ] := 
		       TransposedMat( shreds[ i ][ ii ][ k ][ kk ] );
	      fi;
	    od;
	  od;
	fi;
      od;
    od;
    return shreds;
  end;
  
  # Shred now to save two loop iterations
  Mtrshred := ShredMatrix( Mtr, info.Mstarranges );
  newclass := CVecClass( N[ 1 ], info.Dim ); 
  T:= CVEC_ZeroMat( info.Dim, newclass );
  pairs := Length( info.Mstarranges );
  for i in [ 1 .. pairs ] do
    mi := Length( info.Mstarranges[ i ] );
    ni := Length( info.Nranges[ i ] );
    di := Length( info.Nranges[ i ][ 1 ] );
    for ii in [ 1 .. pairs ] do
      Print( "Doing (",i,", ",ii,") of ", pairs,"^2        \r");
      mii := Length( info.Mstarranges[ ii ] );
      nii := Length( info.Nranges[ ii ] );
      dii := Length( info.Nranges[ ii ][ 1 ] );
      ### catch the special case of scalars ###
      if di = 1 and dii = 1 then
	for l in [ 1 .. ni ] do
	  for ll in [ 1 .. nii ] do
	    Nblock := N[info.Nranges[ i ][ l ][ 1 ]]
		       [info.Nranges[ ii ][ ll ][ 1 ]];
            if not ( Nblock = z0 ) then
	      for k in [ 1 .. mi ] do
		for kk in [ 1 .. mii ] do
		  Mblock := Mtrshred[ ii ][ i ][ kk ][ k ];
		  T[info.Tranges[i][l][k][1]]
		   [info.Tranges[ii][ll][kk][1]] := Mblock * Nblock; 
		od;
	      od;
	    fi;
	  od;
	od;
      else ################################################
	for l in [ 1 .. ni ] do
	  for ll in [ 1 .. nii ] do
	    Nblock := ExtractSubMatrix( N, info.Nranges[ i ][ l ],
					   info.Nranges[ ii ][ ll ] );
	    if not di = 1 then
	      # turn column to row or transpose mat
	      Nblock := TransposedMat( Nblock );
	    fi;
	    for k in [ 1 .. mi ] do
	      for kk in [ 1 .. mii ] do
		Mblock := Mtrshred[ ii ][ i ][ kk ][ k ];
		#Mblock := ExtractSubMatrix( Mtr, info.Mstarranges[ ii ][ kk ],
		#			    info.Mstarranges[ i ][ k ] );
		sum := z0;
		for r in [1 ..Length( Mblock )] do
		  sum := sum + ScalarProduct( Mblock[ r ], Nblock[ r ] );
		od;
		T[info.Tranges[i][l][k][1]]
		 [info.Tranges[ii][ll][kk][1]] := 
		         sum * ( One(BaseField(N)) * dii )^-1; #FIX
		 #TraceMat( Mblock * Nblock );
	      od;
	    od;
	  od;
	od;
      fi;
    od;
  od;
  Print("\n");
  return T;
end);

Testing := function( mat1, mat2, n )
  local z0, mat2tr, runtime, sum1, runtime2, sum2, runtime3, i, r;
  
  z0 := Zero(BaseField(mat1));
  mat2tr := TransposedMat( mat2 );
  runtime:=Runtime();
  #mat1u := List( mat1, Unpack);
  #mat2tru := List( mat2tr, Unpack );
  for i in [1..n] do
    sum1 := z0;
    for r in [1..Length(mat1)] do
      sum1 := sum1 + ScalarProduct( mat1[r],mat2tr[r] );
    od;
  od;
  runtime2 := Runtime();
  for i in [1..n] do
    sum2 := TraceMat( mat1 * mat2 );
  od;
  runtime3:=Runtime();
  return [runtime2-runtime, runtime3 - runtime2, sum1=sum2 ];
end;


InstallGlobalFunction( PreHomcond,
function( M, N )
  local ParseACS, AdaptBasis, isotypesM, isotypesN, isotypepairs, helper, 
        Mranges, Nranges, Bs, CoeffsMinPols, Tranges, dimcount, dim, W, 
	Cmat, mat, tracev, B, TKInfo, i, l, k, didbasechange,
        CompanionMatsSmall, CompanionMatsBig, d;
# INPUT: chop record with ssb of domain (M)
#        chop record with ssb of codomain (N)
#        SHUFFLED!
#        Both where chopped with the SAME database of irreds
#        and if condensation takes place with a non-trivial idempotent,
#        M was replaced by M x Lambda before hand
#        WARNING is destructive: changes bases in chop records
  
  ParseACS:=function( crec )
    # determines the ranges of occurrance of every isotype, ie the row
    # number ranges  of the basis which correspond to a particular isotype
    # SHUFFLED!
    local ranger, currentisotype, dimcount, isotype, i;
    ranger := [ ];
    currentisotype := 0;
    dimcount := 0;
    for i in [ 1 .. Length( crec.acs ) ] do
      isotype := crec.acs[ i ];
      if not ( isotype = currentisotype ) then
	currentisotype := isotype;
	ranger[ currentisotype ] := [ ];
      fi;
      Add( ranger[ currentisotype ], 
      [ dimcount + 1 .. dimcount + Dimension( crec.db[ currentisotype ] ) ] );
      dimcount := dimcount + Dimension( crec.db[ currentisotype ] );
    od;
    return ranger;
  end;

  AdaptBasis:=function( m )
    # adapts basis of a composition factor m to a primitive element of its
    # splitting field.
    # output w needs to be applied to subbasis of large module corresponding
    # to composition factor, i.e. w*[....]
    local e, idword, N, basisN, dim, dimN, counter, basis, imN, k, g, i;
  
    e := DegreeOfSplittingField( m );
    SetWord( m!.wg, IdWordInfo( m ).word );
    idword := EvaluateWord(m!.wg);
    N := SemiEchelonBasisNullspace( 
			Value( IdWordInfo(m).goodfactor, idword ) );
    basisN := Matrix( [N[1]], Length(N[1]), m!.matrices[1]);
    for k in [2..e] do
      basisN[ k ] := basisN[ k - 1 ] * idword;
    od;
    #breadth first spinning
    dim := Dimension( m );
    dimN := Length(N!.vectors);
    counter := 1;
    basis := SemiEchelonBasisMutable( basisN );
    while Length( basis!.vectors ) < dim  do
      for g in m!.matrices do
      #while counter <= Length( basis!.vectors ) and
#	    Length( basis!.vectors ) < dim do
	imN := basisN{ [ counter .. counter + dimN - 1 ] } * g;
	if not CleanRow( basis, ShallowCopy( imN[ 1 ] ), false, fail ) then
	  # Eiertanz, da basis semiechelon sein muss
	  for i in [ 1 .. dimN ] do
	    CleanRow( basis, ShallowCopy( imN[ i ] ), true, fail );
	  od;
	  Append( basisN, imN );
	  if Length( basis!.vectors ) = dim then
	    break;
	  fi;
	fi;
      od;
      counter := counter + dimN;
    od;
    return basisN;
  end;

  isotypesM := Filtered( [1..Length(M.mult)], i->M.mult[i]>0 );
  isotypesN := Filtered( [1..Length(N.mult)], i->N.mult[i]>0 );
  isotypepairs := Intersection( isotypesM, isotypesN );
  #supplemental for backwards compatibility
  helper := List( isotypepairs, x->Position(N.acs,x) );
  SortParallel( helper, isotypepairs );
  
  #
  Mranges := ParseACS( M ){ isotypepairs };
  Nranges := ParseACS( N ){ isotypepairs };
  
  Bs := [];
  CoeffsMinPols := [];
  CompanionMatsSmall := [];
  CompanionMatsBig := [];
  
  Tranges := [];
  dimcount := 0;
  didbasechange := false;
  for i in [ 1 .. Length( isotypepairs ) ] do
    dim := DegreeOfSplittingField( M.db[ isotypepairs[ i ] ] );
    if dim > 1 then
      W := AdaptBasis( M.db[ isotypepairs[ i ] ] );
      # Compute matrix B which stores the traces of the powers of the
      # companion matrix of the primitive element
      if IsZero(IdWordInfo( M.db[ isotypepairs[ i ] ] ).goodfactor ) then
	Cmat:=CompanionMatrix(IdWordInfo(M.db[isotypepairs[i]]).charpoly,
	                      M.db[isotypepairs[i]]!.matrices[1] );
      else
	Cmat:=CompanionMatrix(IdWordInfo(M.db[isotypepairs[i]]).goodfactor,
	                      M.db[isotypepairs[i]]!.matrices[1] );
      fi;
      CompanionMatsSmall[ i ] := Cmat;
      CoeffsMinPols[ i ] := Cmat[ Length( Cmat ) ];
      d := Dimension( M.db[isotypepairs[i]] );
      CompanionMatsBig[ i ] := ZeroMatrix(d,d,Cmat);
      for l in [0,dim..d-dim] do
          CopySubMatrix(Cmat,CompanionMatsBig[i],[1..dim],[l+1..l+dim],
                                                 [1..dim],[l+1..l+dim]);
      od;
      mat := Cmat^0;
      tracev := ZeroVector( 2*dim-1, M.db[isotypepairs[i]]!.matrices[1] );
      for l in [1..2*dim-1] do
	tracev[l] := ( Dimension(M.db[isotypepairs[i]]) / dim ) * 
	               TraceMat( mat );
        mat := mat * Cmat; # room for improvement
      od;
      B := ZeroMatrix( dim, dim, M.db[isotypepairs[i]]!.matrices[1] );
      for l in [ 1 .. dim ] do
	B[ l ] := tracev{ [ l .. dim + l - 1 ] };
      od;
      Bs[ i ] := B^-1;
      # do base change to adapt to splitting field:
      for l in [ 1 .. N.mult[ isotypepairs[ i ] ] ] do
	N.basis{ Nranges[ i ][ l ] } := W * N.basis{ Nranges[ i ][ l ] };
      od;
      if not IsIdenticalObj(M.basis, N.basis) then
        for k in [ 1 .. M.mult[ isotypepairs[ i ] ] ] do
  	  M.basis{ Mranges[ i ][ k ] } := W * M.basis{ Mranges[ i ][ k ] };
	od;
      fi;
      didbasechange := true;
    fi;
    # Now to the Tranges:
    Tranges[ i ] := [];
    for l in [ 1 .. N.mult[ isotypepairs[ i ] ] ] do
      Tranges[ i ][ l ] := [];
      for k in [ 1 .. M.mult[ isotypepairs[ i ] ] ] do
	Tranges[ i ][ l ][ k ] := [ dimcount + 1 .. dimcount + dim ];
	dimcount := dimcount + dim;
      od;
    od;
  od;

  if didbasechange then
    M.ibasis := M.basis^-1;
    if not IsIdenticalObj( M, N ) then
      N.ibasis := N.basis^-1;
    fi;
  fi;

  # prepare output
  TKInfo:=rec();
  TKInfo.isotypepairs:=isotypepairs;
  TKInfo.SplittingFields := List( M.db{isotypepairs},
                                  DegreeOfSplittingField );
  TKInfo.CoeffsMinPols := CoeffsMinPols;
  TKInfo.CompanionMatsSmall := CompanionMatsSmall;
  TKInfo.CompanionMatsBig := CompanionMatsBig;
  TKInfo.Bs := Bs;				  
  TKInfo.Mranges := Mranges;
  TKInfo.Nranges := Nranges;
  TKInfo.Tranges := Tranges;
  TKInfo.Dim := 0;
  for i in isotypepairs do
    TKInfo.Dim := TKInfo.Dim + M.mult[ i ] * N.mult[ i ] * 
                  DegreeOfSplittingField( M.db[ i ] );
  od;
  return TKInfo;

end );


InstallGlobalFunction( Homcond,
function( M, N, info )
  local z0, o0, ShredMatrix, Mshred, newclass, T, pairs, sl, tl, dl, nl, 
        sll, tll, dll, nll, R, L, sum, mat, vec, l, ll, t, tt, s, ss, i, 
	r, bb, ii, jj, v, scaprod, scaprodsrows, entryofmatprod, Rorig;
  # M: matrix action of g on M TRANSPOSED
  # N: matrix action of g on module N
  # BOTH IN SSB! N.B. ssb for contragredient of M!
  # info: info record for Mstar x N
  z0 := Zero( BaseField( N ) );
  o0 := One( BaseField( N ) );
  scaprod := ApplicableMethod( ScalarProduct, [N[1],N[1]] );
  scaprodsrows := ApplicableMethod( ScalarProductsRows, [N,N,Length(N)] );
  entryofmatprod := ApplicableMethod( EntryOfMatrixProduct, [N,N,1,1] );

  ShredMatrix := function( mat, ranges )
    local shreds, di, dii, i, ii, k, kk;
    shreds := [];
    for i in [ 1 .. Length( ranges ) ] do
      shreds[ i ] := [];
      di := Length( ranges[ i ][ 1 ] );
      for ii in [ 1 .. Length( ranges ) ] do
        dii := Length( ranges[ ii ][ 1 ] );
	shreds[ i ][ ii ] := [];
	if di = 1 and dii = 1 then
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] :=
	           mat[ ranges[ i ][ k ][ 1 ] ][ ranges[ ii ][ kk ][ 1 ] ];
	    od;
	  od;
	else
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] := 
		   ExtractSubMatrix( mat, ranges[ i ][ k ],
					  ranges[ ii][ kk ] );
              if dii = 1 and info.SplittingFields[ i ] = 1 and
                             info.SplittingFields[ii] = 1 then
                  shreds[ i ][ ii ][ k ][ kk] := 
                    TransposedMat( shreds[ i ][ ii ][ k ][ kk] );
              fi;
	    od;
	  od;
	fi;
      od;
    od;
    return shreds;
  end;
  
  # Shred now to save two loop iterations
  Mshred := ShredMatrix( M, info.Mranges );
  newclass := CVecClass( N[ 1 ], info.Dim ); 
  T:= CVEC_ZeroMat( info.Dim, newclass );
  pairs := Length( info.Mranges );
  for l in [ 1 .. pairs ] do
    Print( "#I Doing ",l," of ", pairs,"        \r");
    sl := Length( info.Mranges[ l ] );
    tl := Length( info.Nranges[ l ] );
    dl := Length( info.Nranges[ l ][ 1 ] );
    nl := info.SplittingFields[ l ];
    for ll in [ 1 .. pairs ] do
      #Print( "#I Doing (",l,", ",ll,") of ", pairs,"^2        \r");
      sll := Length( info.Mranges[ ll ] );
      tll := Length( info.Nranges[ ll ] );
      dll := Length( info.Nranges[ ll ][ 1 ] );
      nll := info.SplittingFields[ ll ];
      ### catch the special case of scalars ###
      if dl = 1 and dll = 1 then
	for t in [ 1 .. tl ] do
	  for tt in [ 1 .. tll ] do
	    R := N[info.Nranges[ l ][ t ][ 1 ]]
		  [info.Nranges[ ll ][ tt ][ 1 ]];
            if not IsZero( R ) then
	      for s in [ 1 .. sl ] do
                v := T[info.Tranges[l][t][s][1]];
		for ss in [ 1 .. sll ] do
		  L := Mshred[ ll ][ l ][ ss ][ s ];
		  v[info.Tranges[ll][tt][ss][1]] := L * R; 
		od;
	      od;
	    fi;
	  od;
	od;
      else ################################################
	for t in [ 1 .. tl ] do
	  for tt in [ 1 .. tll ] do
	    R := ExtractSubMatrix( N, info.Nranges[ l ][ t ],
					   info.Nranges[ ll ][ tt ] );
            # we no longer transpose R:
	    #if not ( dl = 1 and nll = 1 and nl = 1 ) then
	    #  # turn column to row or transpose mat
	    #  R := TransposedMat( R );
	    #fi;
	    for s in [ 1 .. sl ] do
	      for ss in [ 1 .. sll ] do
		L := Mshred[ ll ][ l ][ ss ][ s ];
		if nll = 1 then 
                  if nl = 1 and dl = 1 then
                    # sum := ScalarProduct(TransposedMat(L)[1],R[1]); #
                    # is already done in ShredMatrix! #
                    sum := scaprod(L[1],R[1]);

                    T[ info.Tranges[ l ][ t ][ s ][ 1 ] ]
                     [ info.Tranges[ ll ][ tt ][ ss ][ 1 ] ] := 
                             sum * ( o0 * dll )^-1;
                  else # nl bel.
                    Rorig := R;
                    for i in [1..nl] do
                      sum := z0;
                      for r in [1 .. dll] do
                        #sum := sum + ScalarProduct( L[ r ], R[ r ] );
                        sum := sum + entryofmatprod( L, R, r, r );
                      od;
                      # was (when R was transposed!):
                      #sum := scaprodsrows( L, R, dll );
                      #
                      T[ info.Tranges[ l ][ t ][ s ][ i ] ]
                       [ info.Tranges[ ll ][ tt ][ ss ][ 1 ] ] := 
                               sum * ( o0 * dll )^-1;
                      if i < nl then
                        R := info.CompanionMatsBig[l] * R;
                        #L := ApplyCompanionMatrix( L, info.CoeffsMinPols[ l ]);
                      fi;
                    od;
                    R := Rorig;
                  fi;
		else # nll > 1
                  Rorig := R;
		  for i in [1..nl] do
		    mat := ZeroMatrix( nll, nll, L );
		    vec := ZeroMutable( mat[1] );
		    for bb in [ 0, nll .. dll - nll ] do
		      for ii in [ 1 .. nll ] do
		        for jj in [ 1 .. nll ] do
		          #vec[jj] := scaprod(L[bb+ii], R[bb+jj]);
		          vec[jj] := entryofmatprod(L,R,bb+ii,bb+jj);
		        od;
		        AddRowVector( mat[ii], vec );
		      od;
		    od;
		    MultRowVector( vec, z0 );
		    vec[ 1 ] := TraceMat( mat );
		    for ii in [2..nll] do
		      #mat := ApplyCompanionMatrix( mat,
		      # 	      info.CoeffsMinPols[ll] );
                      mat := mat * info.CompanionMatsSmall[ll];
		      vec[ii] := TraceMat( mat );
		    od;
		    CopySubVector( vec * info.Bs[ ll ], 
		       	    T[ info.Tranges[ l ][ t ][ s ][ i ] ],
		       	    [1..nll], info.Tranges[ ll ][ tt ][ ss ] );
		    if i < nl then
                      #L := ApplyCompanionMatrix( L, info.CoeffsMinPols[ l ]);
                      R := info.CompanionMatsBig[l] * R;
		    fi;
		  od;
                  R := Rorig;
		fi;
	      od;
	    od;
	  od;
	od;
      fi;
    od;
  od;
  Print("\n");
  return T;
end);

TimeHomcond:=function( arg )
  local timings, db1, rep, m, d, p, f, cl1, D, cl, kgens, mK, mK_ch, module, ibasis, gensinssb, info, x, rt, k, i, l, s;
  # Aufruf: entweder mit Liste von Moduln, Liste von Vielfachheiten oder
  #         Liste von Dimensionen, p, f, Liste von Vielfachheiten
  # Testet Geschwindigkeit des Homcond-Algorithmus. Bei Aufruf mit
  # Modul wird dieser gemaess der gewaehlten Vielfachheit zu einem
  # direkten Produkt zusammengeklebt. Dann wird kondensiert unter der
  # Annahme, dass des Urbild- und Bildmodul bei Einschraenkung auf die
  # Kondensationsuntergruppe in diese direkte Summe zerfallen.
  # Beim Aufruf ohne Modul, wird eine Zufallsmatrix der gegebenen
  # Dimension ueber dem Koerper mit p^f Elementen zu einem direkten
  # Produkt verklebt.
  # Es werden 5 Zufallsmatrizen kondensiert. Die Dauer in Millisekunden
  # ausgegeben.
  # Die gegebene Dimension muss teilerfremd zur Charakteristik des
  # Koerpers sein.
  timings := [];
  if Length( arg ) = 0 then
    Print("#I Usage: either <list modules>, <list multiplicity>\n");
    Print("#I          or   <list d>, <p>, <f>, <list multiplicity>\n");
    return;
  fi;

  if IsModule( arg[1][1] ) then
     db1 := arg[ 1 ];
     rep := List( db1, RepresentingMatrices );
     List( db1, ProperSubmodule );
     List( db1, DegreeOfSplittingField );
     m := arg[ 2 ];
  else
     d := arg[1];
     p := arg[2];
     f := arg[3];
     m := arg[4];
     cl1 := List(d, x->CVecClass( p, f, x ));
     rep := [];
     for k in [1..Length(d)] do
       rep[ k ] := [];
       for i in [1..2] do
         rep[ k ][ i ] := CVEC_ZeroMat(d[k], cl1[ k ]);
         Randomize( rep[ k ][ i ] );
       od;
     od;
     db1 := List( rep, Module );
     for i in [1..Length(db1)] do
       SetIsSimpleModule( db1[i], true );
       SetDegreeOfSplittingField( db1[i], 1 );
     od;
  fi;
  d := List( db1, Dimension );
  D := m*d;
  Print("#I Dimensions: Module = ",D);
  cl := CVecClass( RepresentingMatrices(db1[1])[1][1], D );
  # gefakete kgens
  kgens:=[];

  for i in [1..Length(rep[1])] do
    kgens[ i ] := CVEC_ZeroMat(D, cl);
    for l in [1..Length(rep)] do
      s:=Sum(List([1..l-1],x->d[x]*m[x]));
      for k in [ 1 .. m[ l ] ] do
        CopySubMatrix( rep[ l ][ i ], kgens[ i ], [1..d[l]], 
            [s+(k-1)*d[l]+1.. s + k*d[l]],
            [1..d[l]], [s+(k-1)*d[l]+1.. s + k*d[l]] );
      od;
    od;
  od;
  mK := Module( kgens );
  mK_ch :=rec( ischoprecord:=true,
               db:= db1,
               mult:=m,
               basis := kgens[1]^0,
               acs := Flat(List([1..Length(m)],
                           x->ListWithIdenticalEntries(m[x],x))),
               module := mK,
               ibasis := kgens[1]^0,
               gensinssb := kgens );

  info := PreHomcond( mK_ch, mK_ch );
  Print(", Condensed = ",info.Dim,".\n");
  x := StructuralCopy( kgens[1] );
  for i in [1..5] do
    Randomize(x);
    rt := Runtime();
    Homcond(x,x,info);
    Add( timings, Runtime() - rt );
  od;
  return timings;
end;
