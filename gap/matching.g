#
# the function Furl, Unfurl,Tensormultiply
# all are also found in the chop package file
# chop.gi under similar names.
#
Furl := function( v, m , n )
  local  V, i;
  if m * n <> Length( v ) then
    Error("Dimensions do not match.");
  fi;

  V := CVEC_ZeroMat( m, CVecClass( v, n ) );
  for i  in [ 1 .. m ]  do
      CopySubVector( v, V[i], [ (i - 1) * n + 1 .. i * n ], [ 1 .. n ] );
  od;
  return V;
end;

Unfurl := function( V )
  local  m, n, v, i;
  m := Length( V );
  n := Length( V[1] );
  v := ZeroVector( m * n, V[1] );
  for i  in [ 1 .. m ]  do
      CopySubVector( V[i], v, [ 1 .. n ], [ (i - 1) * n + 1 .. i * n ] );
  od;
  return v;
end;

TensorMultiply := function( v, Atr, B )
  return Unfurl( Atr * Furl( v, Length( Atr[1] ), Length( B ) ) * B );
end;

TensorSpin := function( v, Mtr, N, B, limit )
  local  old, im, counter, i;
  if limit <= Length( B!.vectors )  then
      limit := Length( Mtr[1] ) * Length( N[1] );
  fi;
  old := Length( B!.vectors );
  CleanRow( B, v, true, fail );
  counter := Length( B!.vectors );
  if old = counter  then
      return true;
  fi;
  while counter <= Length( B!.vectors ) and Length( B!.vectors ) < limit  do
      for i  in [ 1 .. Length( Mtr ) ]  do
          im := TensorMultiply( B!.vectors[counter], Mtr[i], N[i] );
          CleanRow( B, im, true, fail );
      od;
      counter := counter + 1;
      Print( "Dimension ", Length( B!.vectors ), ".        \r" );
  od;
  Print( "Dimension ", Length( B!.vectors ), ".\n" );
  return true;
end;

Tuc := function ( v, Mstarch, Nch, tkinfo )
  local mat, src, row, col, dim, colnow, block, i, k, l;
  mat := CVEC_ZeroMat( Dimension( Mstarch.module ), 
     CVecClass( v, Dimension( Mstarch.module ) ) );
  src := 1; #pointer to entries of condensed vector v
  row := 1; #target row
  col := 1; #target column
  for i  in tkinfo.isotypepairs do
      dim := Dimension( Mstarch.db[ i ] );
      for k in [ 1 .. Mstarch.mult[ i ] ] do
        colnow := col;
        for l in [ 1 .. Nch.mult[ i ] ] do
          if dim = 1 then
            mat[ row ][ col ] := v[ src ];
          else
            block := v[src] * One(RepresentingMatrices(Mstarch.db[i])[1]);
            CopySubMatrix( block, mat, [ 1..dim ], [ row..row + dim -1 ],
                           [ 1..dim ], [ col .. col + col - 1 ] );
          fi;
          col := col + dim;
          src := src + 1;
        od;
        col := colnow;
        row := row + dim;
      od;
      col := row;
  od;
  return Unfurl( mat );
end;

TensorActionOnSubspace := function( lgenstr, rgens, basis )
  local dim, zerov, imgens, v, w, i, j;
  # INPUT
  # lgenstr  : List of transposed matrices of left tensor factor
  # rgens  : List of matrices of right tensor factor
  # basis : basis of invariant subspace ie record with fields
  #         pivots   : integer list of pivot columns
  #         vectors : matrix of basis in semi-echelon form
  # OUTPUT
  # List of matrices representing the action of the module given by 'gens'
  # on the Subspace given by 'basis'
  dim:=Length(basis!.vectors);
  zerov := ZeroVector( dim, basis!.vectors[1] ); #prepare vector type
  imgens := []; # stores result
  for i in [ 1 .. Length( rgens ) ] do
    imgens[ i ] := ZeroMatrix(dim, dim, rgens[1] );
    for j in [1..dim] do
      v := basis!.vectors[ j ];
      w := ShallowCopy( zerov );
      CleanRow( basis, Unfurl (lgenstr[ i ] * Furl( v,Length(lgenstr[1]),
                Length(rgens[1])) * rgens[ i ] ), false, w );
      CopySubVector( w, imgens[ i ][ j ], [1..dim], [1..dim] );
    od;
  od;
  return imgens;
end;

FindPeakword := function( nr, db)
  local m, e, savedwordm, algel, cpfacs, facsdege, factoroptions, x, N, pw, m2, savedword,
 algel2, x2, N2, peakword, fac, i, k;
# finds a peakword for the simple module m with respect to the database db
# in other words, finds an idword with stable kernel on m, which has nullity
# 0 on all other simples in the database
  
  m := db[ nr ];
  e := DegreeOfSplittingField( m );
  #Print(CurrentWord(m!.wg),"\n");
  savedwordm := CurrentWord(m!.wg);
  while true do
      NextWord( m!.wg );
      algel := EvaluateWord( m!.wg );
      cpfacs := CharacteristicPolynomialOfMatrix( algel );
      facsdege := [];
      for fac in cpfacs.factors do
          Append(facsdege, 
          Factors( PolynomialRing(BaseField( algel ) ), 
                   fac : factoroptions := rec( onlydegs := [e] ) ));
      od;
      if Length(facsdege) = 0 then
          continue;
      fi;
      facsdege := Collected(facsdege);
      for i in [ 1..Length( facsdege ) ] do
          x := Value( facsdege[ i ][ 1 ], algel );
          N := NullspaceMatMutable( x );
          if Length( N ) = e then
              # Found idword
              #Print("Found idword...\n");
              # check if kernel is stable
              if Length( NullspaceMatMutable( x * x ) ) = e then
                #Print("   ...its kernel is stable...\n");
                # kernel is stable
                # check on all other simples
                for k in [ 1 .. Length( db ) ] do
                  pw := true;
                  if k = nr then
                    continue;
                  else
                    m2 := db[ k ];
                    savedword:=CurrentWord( m2!.wg ); 
                    SetWord( m2!.wg, m!.wg!.word ); 
                    algel2 := EvaluateWord( m2!.wg );
                    SetWord( m2!.wg, savedword );
                    x2 := Value( facsdege[ i ][ 1 ], algel2 );
                    N2 := NullspaceMatMutableX( x2 );
                    if Length( N2 ) <> 0 then
                      #Print("   ...NOT a peakword (mismatch on ",k," !)\n");
                      # no peakword for m
                      pw := false;
                      break;
                    fi;
                  fi;
                od;
                
                if pw = false then
                  break; # take next poly
                else
                  #Print("   ...it IS a peakword.\n");
                  # peakword found
                  peakword := CurrentWord( m!.wg );
                  SetWord( m!.wg, savedwordm );
                  return rec( peakword:=peakword,
                              goodfactor := facsdege[i][1] );
                fi;
              fi;
          fi;
      od;
  od;
end;

PeakwordEvaluater:=function(pw, n)
  local a, x;
  SetWord(n!.wg, pw.peakword);
  a:=EvaluateWord(n!.wg);
  x:=Value(pw.goodfactor, a);
  return x;
end;

HomUncond := function ( v, info )
  local mat, o0, src, sl, tl, dl, nl, block, comp, Block, l, t, s, i, b;
  mat := CVEC_ZeroMat( info.Mdim, CVecClass( v, info.Ndim ) );
  o0 := One(BaseField( v ) );
  src := 1; #pointer to entries of condensed vector v
  #for i  in info.isotypepairs do
  for l  in [1..Length(info.Mranges)] do
    #Print("\n#I ",l,"\n");
    sl := Length( info.Mranges[ l ] ); # ranges in Domain of CF isomorphic to 
    				       #  paired isotype l = Mmult
    tl := Length( info.Nranges[ l ] ); # ranges in Codomain of CF iso to
    				       #  paired isotype l = Nmult
    dl := Length( info.Nranges[ l ][ 1 ] ); # Dim of isotpype l
    nl := info.SplittingFields[ l ]; # splitting field of isotype l
   
    for t in [ 1 .. tl ] do
      for s in [ 1 .. sl ] do
	if nl = 1 then #splitting case
	      if not IsZero( v[src] ) then
		if dl = 1 then
		  mat[ info.Mranges[l][s][1] ][ info.Nranges[l][t][1] ] := v[ src ];
		else
		  block := v[src] * IdentityMatrix( dl, mat );
		  CopySubMatrix( block, mat, [ 1..dl ], info.Mranges[l][s],
				 [ 1..dl ], info.Nranges[l][t] );
		fi;
	      fi;
	else # non-splitting
	      comp := ZeroMatrix( nl, nl, mat );
	      CopySubMatrix( IdentityMatrix( nl - 1, mat ), comp, [1..nl-1],
					   [1..nl-1], [1..nl-1], [2..nl] );
	      CopySubVector( info.CoeffsMinPols[ l ], comp[ nl ], [1..nl], [1..nl] );
	      block := v[ src + nl - 1 ] * One(comp);
	      for i in [nl-2, nl-3..0] do
		block := v[ src + i ] * One(comp) + block * comp;
	      od;
	      Block := ZeroMatrix( dl, dl, mat );
	      for b in [0, nl .. dl - nl ] do
		CopySubMatrix( block, Block, [1..nl], [b+1..b+nl], [1..nl],
		[b+1..b+nl] );
	      od;
	      CopySubMatrix( Block, mat, [1..dl], info.Mranges[l][s], [1..dl],
	      info.Nranges[l][t] );

	fi;
	src := src + nl;
      od;
    od;
  od;
  return mat;
end;

ProjHomcond:=function( V, info )
  local T, indx, o0, sl, tl, dl, nl, m, mat, vec, l, t, s, i, b;
  # V is a vector of Hom(M,N). Should be given furled.
  T:= ZeroVector( info.Dim, V );
  # The result matrix T should now be a vector
  indx := 1;
  o0 := One( BaseField( V ) );
  for l in [ 1 .. Length( info.Mranges ) ] do
    sl := Length( info.Mranges[ l ] ); # ranges in Domain of CF isomorphic to 
    				       #  paired isotype l = Mmult
    tl := Length( info.Nranges[ l ] ); # ranges in Codomain of CF iso to
    				       #  paired isotype l = Nmult
    dl := Length( info.Nranges[ l ][ 1 ] ); # Dim of isotpype l
    nl := info.SplittingFields[ l ]; # splitting field of isotype l

    if nl = 1 then # splitting field case
      for t in [ 1 .. tl ] do
	for s in [ 1 .. sl ] do
	  if dl = 1 then
	   T[ indx ] :=
	   V[ info.Mranges[ l ][ s ][1] ][ info.Nranges[ l ][ t ][1] ];
	 else # trace is not optimal
	  T[ indx ] := ( o0 * dl )^-1 *
	  TraceMat( ExtractSubMatrix( V, info.Mranges[ l ][ s ],
				      info.Nranges[ l ][ t ] ) );
	 fi;
	 indx := indx + nl;
	od;
      od;
    else # non-splitting case
      # do some stuff
      for t in [1..tl] do
	for s in [1..sl] do
	  m := ExtractSubMatrix( V, info.Mranges[ l ][ s ],
				    info.Nranges[ l ][ t ] );
          mat := ZeroMatrix( nl, nl, m );
	  vec := ZeroMutable( mat[1] );
	  for b in [0, nl .. dl - nl ] do
	    mat := mat + ExtractSubMatrix( m, [b+1..b+nl], [b+1..b+nl] );
	  od;
	  #od;
	  vec[ 1 ] := TraceMat( mat );
	  for i in [2..nl] do
	    mat := ApplyCompanionMatrix( mat, info.CoeffsMinPols[l] );
	    vec[ i ] := TraceMat( mat );
	  od;
	  CopySubVector( vec * info.Bs[ l ], T, [1..nl], [ indx .. indx + nl - 1 ] );
	  indx := indx + nl;
	od;
      od;
    fi;
  od;
  return T;
end;

StableKernelsOfPeakwords := function( pwlist, mch )
  local vvv, w, w2, d, count, nsp, d2, i;
 vvv:=[];;
 for i in [1..Length(pwlist)] do
  Print("#I",i,":\n");
  w := PeakwordEvaluater( pwlist[i], mch.module );
  w2 := One(w);
  d := 0;
  count := 1;
  #while true do
  while d < mch.mult[ i ] * DegreeOfSplittingField( mch.db[ i ] ) do
   w2 := w2 * w;
   Print("#I power ",count,":");
   nsp := NullspaceMat( w2 );
   #d2 := Length( nsp );
   d := Length( nsp );
   #Print(" dim = ",d2,"\r");
   Print(" dim = ",d,"\r");
   #if d2 = d then
   # break;
   #fi;
   #d := d2;
   count := count + 1;
  od;
  Print("\n");
  Add(vvv, nsp );
 od;
 return vvv;
end;

PartialKernelsOfPeakwords := function( pwlist, mch )
  local vvv, w, i;
 vvv:=[];;
 for i in [1..Length(pwlist)] do
  Print("#I",i,":\n");
  w := PeakwordEvaluater( pwlist[i], mch.module );
  Add(vvv, NullspaceMat( w ) );
 od;
 return vvv;
end;


HC_MatchConstituentViaVector:=function( m, m2, v, DB, minfo1, minfo2, knowndim )
  local HC_ZigZagVector, localtime, totaltime, spintime, basistime, 
        actiontime, socletime, uctime, basis, rt, dim, vv, gens, mt, 
	dbt, soc, mm;
  # m: condensed module
  # m2: condensed module in other condensation
  # v: peakword kernel vector
  # DB: database of candidates
  # minfo1: native condinfo of v PLUS
  #        ssb for domain and codomain
  # minfo2: condinfo of other condensation
  #        PLUS ssb for domain and codomain
  # knowndim: Dimension of space generated by v, if known
  #           set to 0 if unkown
  HC_ZigZagVector:=function( v, minfo1, minfo2 )
    local vv, vv2;
    # v: peakword kernel vector
    # minfo1: native condinfo of v PLUS
    #        ssb for domain and codomain
    # minfo2: condinfo of other condensation
    #        PLUS ssb for domain and codomain
    Print("#I Uncondensing vector...\c");
    rt:=Runtime();
    vv:=HomUncond( v, minfo1 );
    localtime := Runtime() -rt;
    uctime := uctime + localtime;
    Print("done. ", localtime,"\n\c");
    Print("#I Changing basis...\c");
    rt:=Runtime();
    vv:=minfo2.Mbas * minfo1.Mbasi * vv * minfo1.Nbas * minfo2.Nbasi;
    localtime := Runtime() -rt;
    basistime := basistime + localtime;
    Print("done. ", localtime,"\n\c");
    Print("#I Projecting onto condensed module...\c");
    rt:=Runtime();
    vv:=ProjHomcond( vv, minfo2 );
    localtime := Runtime() -rt;
    uctime := uctime + localtime;
    Print("done. ", localtime,"\n\c");
    Print("#I Uncondensing vector...\c");
    rt:=Runtime();
    vv2:=HomUncond( vv, minfo2 );
    localtime := Runtime() -rt;
    uctime := uctime + localtime;
    Print("done. ", localtime,"\n\c");
    Print("#I Changing basis...\c");
    rt:=Runtime();
    vv2:=minfo1.Mbas * minfo2.Mbasi * vv2 * minfo2.Nbas * minfo1.Nbasi;
    localtime := Runtime() -rt;
    basistime := basistime + localtime;
    Print("done. ", localtime,"\n\c");
    Print("#I Projecting onto condensed module...\c");
    rt:=Runtime();
    vv2:=ProjHomcond( vv2, minfo1 );
    localtime := Runtime() -rt;
    uctime := uctime + localtime;
    Print("done. ", localtime,"\n\c");
    return [vv2, vv]; #[ProjHomcond( vv2, minfo1 ), vv];
  end;
  
  localtime := 0; # stores various times
  totaltime:=Runtime(); #total runtime
  spintime := 0; # time spent spinning
  basistime := 0; # time spent changing bases
  actiontime := 0; # time spent on action
  socletime := 0; # time spent on socle
  uctime := 0; #time spent un/condensing

  if knowndim = 0 then
    Print("#I Determining target dimension...\c");
    basis:=EmptySemiEchelonBasis( RepresentingMatrices(m)[1] );
    rt:=Runtime();
    CH0P_Spin( RepresentingMatrices(m), basis, Dimension(m),
               ShallowCopy(v));
    localtime := Runtime() -rt;
    spintime := spintime + localtime;
    dim := Length(basis!.vectors);
    Print(" it is ",dim,". ",localtime,"\n\c");
  else
    dim := knowndim;
  fi;
  vv :=HC_ZigZagVector( v, minfo1, minfo2 );
  basis:=EmptySemiEchelonBasis( RepresentingMatrices(m)[1] );
  rt:=Runtime();
  CH0P_Spin( RepresentingMatrices(m), basis, dim, ShallowCopy(vv[1]));
  localtime := Runtime() -rt;
  spintime := spintime + localtime;
  if Length( basis!.vectors ) <> dim then
    Print("#I No match is possible!\n\c");
    return fail;
  else
    Print("#I Finding match...\n\c");
    basis:=EmptySemiEchelonBasis( RepresentingMatrices( m2 )[1] );
    rt:=Runtime();
    CH0P_Spin( RepresentingMatrices(m2), basis, Dimension(m2),
               ShallowCopy(vv[2]) );
    localtime := Runtime() -rt;
    spintime := spintime + localtime;
    Print("#I Computing action on subspace of dimension ",
           Length(basis!.vectors),"...\c");
    rt:=Runtime();
    gens:=CH0P_ActionOnSubspace( RepresentingMatrices(m2), basis );
    localtime := Runtime() -rt;
    actiontime := actiontime + localtime;
    Print("done. ", localtime,"\n\c");
    mt := Module(List(gens,TransposedMat));
    dbt := List(DB,DualModule);
    for mm in dbt do
      TransformToStandardBasis(mm);
    od;
    Print("#I Seeking socle of dual...\c");
    rt:=Runtime();
    soc := SocleOfModule(mt,dbt);
    localtime := Runtime() -rt;
    socletime := socletime + localtime;
    Print("done. ", localtime,"\n\c");
    Print("#I total, spin, basis, uc, socle, action :\n\c");
    Print("#I ",Runtime()-totaltime,", ",spintime,", ", 
           basistime, ", ", uctime);
    Print(", ", socletime,", ",actiontime,"\n\c");
    return soc.isotypes[1]; #DB[ soc.isotypes[1] ];
  fi;
end;

GeneratorsPIMs := function( pwlist, mch )
  local vvv, w, w2, d, goal, nsp1, nsp2, i, j;
  vvv := [];;
  for i in [1..Length(pwlist)] do
    Print("i=",i," of ",Length(pwlist),".\r");
    Add( vvv, [] );
    w := PeakwordEvaluater( pwlist[ i ], mch.module );
    w2 := One( w );
    d := 0;
    goal := mch.mult[ i ] * DegreeOfSplittingField( mch.db[ i ] );
    if IsBound( nsp2 ) then
      Unbind( nsp2 );
    fi;
    while d < goal do
      w2 := w2 * w;
      nsp1 := SemiEchelonBasisNullspace( w2 );
      d := Length( nsp1 );
      if d = goal then
        if IsBound( nsp2 ) then
          for j in [ 1 .. Length( nsp2 ) ] do
            CleanRow( nsp2, Vectors( nsp1 )[ j ], false, fail );
            if Vectors( nsp1 )[ j ] <> Zero( Vectors( nsp1 )[ j ] ) then
              Add( vvv[ i ], Vectors( nsp1 )[ j ] );
            fi;
          od;
          vvv[ i ] := CMat( vvv[ i ] );
        else
          vvv[ i ] := Vectors( nsp1 );
        fi;
        break;
      fi;
      nsp2 := StructuralCopy( nsp1 );
    od;
  od;
  return vvv;
end;

TopTensorSpinActSimul:=function( lgenstr, rgens, bottombas, dim, seed )
  local topbas, act, one, counter, im, dec, inspan, k;
  # given a semiechelonbasis of a submodule, we spin up the seed and
  # simultaneously compute the matrices giving the action on the quot.

  #1st clean with bottombas, do not append
  #2nd clean with topbas and append
  topbas := EmptySemiEchelonBasis( rgens[1] );
  act := [];
  one := One( BaseField( seed ) );
  for k in [1..Length(rgens)] do
    Add(act, CVEC_ZeroMat( dim, CVecClass( seed, dim ) ) );
  od;
  if CleanRow( bottombas, seed, false, fail ) then
    Print("#I Seed lies in subspace, you tool!\n");
    return true;
  fi;
  CleanRow( topbas, seed, true, fail ); #Append seed to topbas
  counter := 1;
  while counter <= Length( topbas!.vectors ) do
    Print("#I Counter ", counter, ", Dimension ",Length(topbas!.vectors),"\r");
    for k in [1..Length(rgens)] do
      im := Unfurl( lgenstr[ k ] * 
            Furl( topbas!.vectors[ counter ], Length(lgenstr[1]),
            Length(rgens[1]) ) * rgens[ k ] );
      CleanRow( bottombas, im, false, fail );
      dec := ZeroVector( dim, topbas!.vectors );
      CleanRow( topbas, im, true, dec );
      act[ k ][ counter ] := ShallowCopy( dec );
#      CopySubVector( dec, act[ k ][ counter ], [1..dim],
#                     [1..dim] );
    od;
    counter := counter + 1;
  od;
  Print("\n");
  return act;
end;

PUncond:=function(v, subooo)
  local uv, ptr, i, j;
  uv := ZeroVector( Sum(subooo.lens), v );
  ptr := 0;
  for i in [1..Length(v)] do
    for j in [1..subooo.lens[i]] do
      ptr := ptr + 1;
      uv[ ptr ] := v[ i ] * (subooo.lens[i]^-1);
    od;
  od;
  return uv;
end;

