DoworkIdem := function( t, res, subres )
  
  if res = fail then
    return subres;
  else
    return res + subres * t;
  fi;
end;

PermutedSummands := function(re,plan)
  local newgens, basperm, pos, row, dim, i, k, j;
  # re a CHOP-record after SemiSimplicityBasis 
  # plan a list of length re.acs, containing the numbers of summands in
  # the order as they should appear in the resulting module.
  # Changes re in place, updates re.gensinssb.

  if Set(plan) <> [1..Length(re.acs)] then
      Error("Plan must describe a permutation!");
  fi;
  newgens := []; #FIXME Speicher!
  for i in [1..Length(re.gensinssb)] do
    Add( newgens, ZeroMutable( re.gensinssb[ 1 ] ) );
  od;
  
  basperm := [];   # Here we collect the basis permutation, such that in
                   # the end, we have to do:
                   #   basnew := bas{basperm}
  pos := 0;
  
  for i in plan do
    row := 0;
    for k in [1..i-1] do
      row := row + Dimension( re.db[ re.acs[ k ] ] );
    od;
    dim := Dimension( re.db[ re.acs[ i ] ] );
      for j in [1..Length(re.gensinssb)] do
	CopySubMatrix( re.gensinssb[j], newgens[j], 
	               [ row + 1 .. row + dim ],
	               [ pos + 1 .. pos + dim ],
	               [ row + 1 .. row + dim ],
	               [ pos + 1 .. pos + dim ] );
        basperm{ [ pos + 1 .. pos + dim ] } := [ row + 1 .. row + dim ];
      od;
      pos := pos + dim;
  od;
  re.gensinssb := newgens;
  re.basis := re.basis{basperm};
  for i in [1..Length(re.ibasis)] do
    re.ibasis[i] := re.ibasis[i]{basperm};
  od;
  return re;
end;

Shuffle:=function(re)
  # Sorts output of direct sum chop, st basis is ssb
  local OurSorter, OurSorter2, plan, planCF;
  OurSorter := function(a,b)
    if Dimension( re.db[ re.acs[ a ] ] ) <
       Dimension( re.db[ re.acs[ b ] ] ) then
      return true;
    elif Dimension( re.db[ re.acs[ a ] ] ) >
         Dimension( re.db[ re.acs[ b ] ] ) then
      return false;
    else
      return re.acs[ a ] < re.acs[ b ];
    fi;
  end;
  plan := [ 1 .. Length( re.acs ) ];
  Sort( plan, OurSorter );
  PermutedSummands( re, plan );
  re.acs:=re.acs{ plan };
end;


PreTcond := function( Mstar, N )
# INPUT: chop record with ssb of CONTREGRADIENT of left factor (M)
#        chop record with ssb of right factor (N)
#        SHUFFLED! (for the beauty of it)
#        Both where chopped with the SAME database of irreds
  local ParseACS, isotypesMstar, isotypesN, isotypepairs, Mstarranges, 
        Nranges, Tranges, dimcount, dim, TKInfo, helper, i, l, k;
  
  ParseACS:=function( crec )
    # determines the ranges of occurrance of every isotype, ie the row
    # number ranges  of the basis which correspond to a particular isotype
    local ranger, currentisotype, dimcount, isotype, i;
    ranger := [ ];
    currentisotype := 0;
    dimcount := 0;
    for i in [ 1 .. Length( crec.acs ) ] do
      isotype := crec.acs[ i ];
      if not ( isotype = currentisotype ) then
	currentisotype := isotype;
	ranger[ currentisotype ] := [ ];
      fi;
      Add( ranger[ currentisotype ], 
      [ dimcount + 1 .. dimcount + Dimension( crec.db[ currentisotype ] ) ] );
      dimcount := dimcount + Dimension( crec.db[ currentisotype ] );
    od;
    return ranger;
  end;

  isotypesMstar := Filtered( [1..Length(Mstar.mult)],
		              i->Mstar.mult[i]>0 );
  isotypesN := Filtered( [1..Length(N.mult)], i->N.mult[i]>0 );
  isotypepairs := Intersection( isotypesMstar, isotypesN );
  #supplemental for backwards compatibility
  helper := List( isotypepairs, x->Position(N.acs,x) );
  SortParallel( helper, isotypepairs );
  
  #
  Mstarranges := ParseACS( Mstar ){ isotypepairs };
  Nranges := ParseACS( N ){ isotypepairs };
  
  
  
  Tranges := [];
  dimcount := 0;
  for i in [ 1 .. Length( isotypepairs ) ] do
    dim := DegreeOfSplittingField(Mstar.db[isotypepairs[i]]);
    Tranges[ i ] := [];
    for l in [ 1 .. N.mult[ isotypepairs[ i ] ] ] do
      Tranges[ i ][ l ] := [];
      for k in [ 1 .. Mstar.mult[ isotypepairs[ i ] ] ] do
	Tranges[ i ][ l ][ k ] := [dimcount+1..dimcount+dim];
	dimcount := dimcount + dim;
      od;
    od;
  od;
  TKInfo:=rec();
  TKInfo.isotypepairs:=isotypepairs;
  #
  #TKInfo.CfIndexM:=isotypepairs;
  #TKInfo.CfIndexN:=isotypepairs;
  #TKInfo.NCf := Length( isotypepairs );
  #
  TKInfo.SplittingFields := List( Mstar.db{isotypepairs},
                                  DegreeOfSplittingField );
				  
  TKInfo.Mstarranges := Mstarranges;
  TKInfo.Nranges := Nranges;
  TKInfo.Tranges := Tranges;
  TKInfo.Dim := 0;
  for i in isotypepairs do
    TKInfo.Dim := TKInfo.Dim + Mstar.mult[ i ] * N.mult[ i ] * 
                  DegreeOfSplittingField( Mstar.db[ i ] );
  od;
  return TKInfo;
end;

Tcond := function( Mtr, N, info )
  local z0, ShredMatrix, Mtrshred, newclass, T, pairs, mi, ni, di, mii, nii, dii, Nblock, Mblock, sum, i, ii, l, ll, k, kk, r;
  # M: matrix action of g on M TRANSPOSED
  # N: matrix action of g on module N
  # BOTH IN SSB! N.B. ssb for contragredient of M!
  # info: info record for Mstar x N
  z0 := Zero( BaseField( N ) );
  ShredMatrix := function( mat, ranges )
    local shreds, di, dii, i, ii, k, kk;
    shreds := [];
    for i in [ 1 .. Length( ranges ) ] do
      shreds[ i ] := [];
      di := Length( ranges[ i ][ 1 ] );
      for ii in [ 1 .. Length( ranges ) ] do
        dii := Length( ranges[ ii ][ 1 ] );
	shreds[ i ][ ii ] := [];
	if di = 1 and dii = 1 then
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] :=
	           mat[ ranges[ i ][ k ][ 1 ] ][ ranges[ ii ][ kk ][ 1 ] ];
	    od;
	  od;
	else
	  for k in [ 1 .. Length( ranges[ i ] ) ] do
	    shreds[ i ][ ii ][ k ] := [];
	    for kk in [ 1 .. Length( ranges[ ii ] ) ] do
	      shreds[ i ][ ii ][ k ][ kk ] := 
		   ExtractSubMatrix( mat, ranges[ i ][ k ],
					  ranges[ ii][ kk ] );
	      if dii = 1 then
	        shreds[ i ][ ii ][ k ][ kk ] := 
		       TransposedMat( shreds[ i ][ ii ][ k ][ kk ] );
	      fi;
	    od;
	  od;
	fi;
      od;
    od;
    return shreds;
  end;
  
  # Shred now to save two loop iterations
  Mtrshred := ShredMatrix( Mtr, info.Mstarranges );
  newclass := CVecClass( N[ 1 ], info.Dim ); 
  T:= CVEC_ZeroMat( info.Dim, newclass );
  pairs := Length( info.Mstarranges );
  for i in [ 1 .. pairs ] do
    mi := Length( info.Mstarranges[ i ] );
    ni := Length( info.Nranges[ i ] );
    di := Length( info.Nranges[ i ][ 1 ] );
    for ii in [ 1 .. pairs ] do
      Print( "Doing (",i,", ",ii,") of ", pairs,"^2        \r");
      mii := Length( info.Mstarranges[ ii ] );
      nii := Length( info.Nranges[ ii ] );
      dii := Length( info.Nranges[ ii ][ 1 ] );
      ### catch the special case of scalars ###
      if di = 1 and dii = 1 then
	for l in [ 1 .. ni ] do
	  for ll in [ 1 .. nii ] do
	    Nblock := N[info.Nranges[ i ][ l ][ 1 ]]
		       [info.Nranges[ ii ][ ll ][ 1 ]];
            if not ( Nblock = z0 ) then
	      for k in [ 1 .. mi ] do
		for kk in [ 1 .. mii ] do
		  Mblock := Mtrshred[ ii ][ i ][ kk ][ k ];
		  T[info.Tranges[i][l][k][1]]
		   [info.Tranges[ii][ll][kk][1]] := Mblock * Nblock; 
		od;
	      od;
	    fi;
	  od;
	od;
      else ################################################
	for l in [ 1 .. ni ] do
	  for ll in [ 1 .. nii ] do
	    Nblock := ExtractSubMatrix( N, info.Nranges[ i ][ l ],
					   info.Nranges[ ii ][ ll ] );
	    if not di = 1 then
	      # turn column to row or transpose mat
	      Nblock := TransposedMat( Nblock );
	    fi;
	    for k in [ 1 .. mi ] do
	      for kk in [ 1 .. mii ] do
		Mblock := Mtrshred[ ii ][ i ][ kk ][ k ];
		#Mblock := ExtractSubMatrix( Mtr, info.Mstarranges[ ii ][ kk ],
		#			    info.Mstarranges[ i ][ k ] );
		sum := z0;
		for r in [1 ..Length( Mblock )] do
		  sum := sum + ScalarProduct( Mblock[ r ], Nblock[ r ] );
		od;
		T[info.Tranges[i][l][k][1]]
		 [info.Tranges[ii][ll][kk][1]] := 
		         sum * ( One(BaseField(N)) * dii )^-1; #FIX
		 #TraceMat( Mblock * Nblock );
	      od;
	    od;
	  od;
	od;
      fi;
    od;
  od;
  Print("\n");
  return T;
end;

Testing := function( mat1, mat2, n )
  local z0, mat2tr, runtime, sum1, runtime2, sum2, runtime3, i, r;
  
  z0 := Zero(BaseField(mat1));
  mat2tr := TransposedMat( mat2 );
  runtime:=Runtime();
  #mat1u := List( mat1, Unpack);
  #mat2tru := List( mat2tr, Unpack );
  for i in [1..n] do
    sum1 := z0;
    for r in [1..Length(mat1)] do
      sum1 := sum1 + ScalarProduct( mat1[r],mat2tr[r] );
    od;
  od;
  runtime2 := Runtime();
  for i in [1..n] do
    sum2 := TraceMat( mat1 * mat2 );
  od;
  runtime3:=Runtime();
  return [runtime2-runtime, runtime3 - runtime2, sum1=sum2 ];
end;

SemiSimplicityBasisByIdempotentsHNmod5:=
  function( m, db, chain, cfstart, pterstart )
  local gens, maxdim, basis, pter, o1, collectid, DirtyThing, 
        chainrepsbig, bigone, BuildId, id, workid, bas, goodrows, 
	v, newgoodrows, i, j;
  # we compute a semiplicity basis for the module m with the help
  # of the primitive idempotents corresp to its Cfs
  # db is a full set of isotypes of the Cfs of m
  gens := GeneratorsOfModule( m );
  maxdim := Length( gens[1] );
  #basis := CVEC_ZeroMat( maxdim, CVecClass(gens[ 1 ][ 1 ]) );
  basis := CVEC_ReadMatFromFile("/export3/home/tmp/noeske/HN/8152part.ssb");
  pter := pterstart;
  o1 := One( BaseField( m  ) );
  #collectid := CVEC_ZeroMat( maxdim, CVecClass(gens[ 1 ][ 1 ]) );
  collectid :=
    CVEC_ReadMatFromFile("/export3/home/tmp/noeske/HN/collectid");
  
  DirtyThing := function( reps )
    local list;
    Print("Computing transversal in module of dimension ",
          Length( reps[ 1 ] ),"...\n");
    list := [];
    Add( list, reps[1] );
    Add( list, reps[2]*reps[1]*reps[2] );
    list := Concatenation( list, reps{[2..8]} );
    return list;
  end;
  
  #chainrepsbig := DirtyThing( GeneratorsOfModule( m ) );
  chainrepsbig := 
    CVEC_ReadMatsFromFile("/export3/home/tmp/noeske/HN/chainreps8152.");
  bigone := One(chainrepsbig[1]);
  
  BuildId := function( smallm )
    local chainrepssmall, id, x, scalar, sum, i;
    Print("Building idempotent...\n");
    chainrepssmall := DirtyThing( GeneratorsOfModule( smallm ) );
    id := bigone;
    for i in [ 1 .. Length( chainrepssmall ) ] do
      Print(".\c");
      x := chainrepssmall[ i ]^-1;
      scalar := x[ 1 ][ 1 ];
      if not IsZero( scalar ) then
	sum := bigone + scalar * chainrepsbig[ i ];
	id := id * sum;
      fi;
    od;
    Print("\n");
    return ( o1 * Size( chain ) )^-1 * id;
  end;
  
  for i in [cfstart..Length(db)] do
    if Dimension( db[ i ] ) = 1 then
      Print("Treating CF ",i," of ",Length(db),"\n");
      # build id
      id := BuildId( db[ i ] );
      #Error(1);
      #if not ( id * id = id ) then
	#Error("\nNOT an idempotent!.\n");
      #fi;
      collectid := collectid + id;
      workid := ShallowCopy( id ); # this will be destroyed
      goodrows := Filtered( [1..maxdim], 
		             x->PositionNonZero( id[ x ] ) < maxdim + 1 );
      while Length( goodrows ) > 0 do
	Print("pter = ",pter,"\n");
	# get first nonzero row vector from id
	v := ShallowCopy( id[ goodrows[ 1 ] ] ); # dont destroy it!
	# Spin up v
        bas := EmptySemiEchelonBasis( gens[ 1 ][ 1 ] ); #basis for subspace
	CH0P_Spin( gens, bas, maxdim, v );
	# copy subspace basis into big basis
	CopySubMatrix( bas.vectors, basis, [1..Length(bas.vectors)], 
	               [pter..pter+Length(bas.vectors)-1],
				   [1..maxdim], [1..maxdim] );
	pter := pter + Length(bas.vectors);
	# clean id with spc and repeat
	newgoodrows := [];
	for j in goodrows do
	  if not CleanRow( bas, workid[ j ], false, fail ) then
	    Add( newgoodrows, j );
	  fi;
	od;
	goodrows := newgoodrows;
      od;
    fi;
  od;
  Print("Saving...\n");
  CVEC_WriteMatToFile("/export3/home/tmp/noeske/HN/collectid", collectid);
  Print("Doing final non-linear idempotent...\n");
  id := collectid^0 - collectid;
  workid := ShallowCopy(id);
  goodrows := Filtered( [1..maxdim], 
	                x->PositionNonZero( id[ x ] ) < maxdim+1 );
  while Length( goodrows ) > 0 do
    Print("pter = ",pter,"\n");
    # get first nonzero row vector from id
    v := ShallowCopy( id[ goodrows[ 1 ] ] ); # dont destroy it!
    bas := EmptySemiEchelonBasis( gens[ 1 ][ 1 ] ); #basis for subspace
    # Spin up v
    CH0P_Spin( gens, bas, maxdim, v );
    # copy subspace basis into big basis
    CopySubMatrix( bas.vectors, basis, [1..Length(bas.vectors)], 
                   [pter..pter+Length(bas.vectors)-1],
			       [1..maxdim], [1..maxdim] );
    pter := pter + Length(bas.vectors);
    # clean id with spc and repeat
    newgoodrows := [];
    for j in goodrows do
      if not CleanRow( bas, workid[ j ], false, fail ) then
	Add( newgoodrows, j );
      fi;
    od;
    goodrows := newgoodrows;
  od;
  return basis;
end;


SemiSimplicityBasisByIdempotentsHNmod5LastPart:=
  function( m, iso )
  local gens, maxdim, basis, pter, o1, collectid, DirtyThing, 
        chainrepsbig, bigone, BuildId, id, workid, bas, goodrows, 
	v, newgoodrows, i, j, interbasis, script;
  # we compute a semiplicity basis for the module m with the help
  # of the primitive idempotents corresp to its Cfs
  # db is a full set of isotypes of the Cfs of m
  gens := GeneratorsOfModule( m );
  maxdim := Length( gens[1] );
  #basis := CVEC_ZeroMat( maxdim, CVecClass(gens[ 1 ][ 1 ]) );
  basis := CVEC_ReadMatFromFile("/export3/home/tmp/noeske/HN/guck.sav");
  pter := 4057;
  o1 := One( BaseField( m  ) );
  #collectid := CVEC_ZeroMat( maxdim, CVecClass(gens[ 1 ][ 1 ]) );
  collectid :=
    CVEC_ReadMatFromFile("/export3/home/tmp/noeske/HN/collectid");
  Print("Doing final non-linear idempotent...\n");
  workid := SemiEchelonRowsX(
	    CVEC_ReadMatFromFile("/export3/home/tmp/noeske/HN/primid16.8152") );
#  id:=CVEC_ReadMatFromFile("/export3/home/tmp/noeske/HN/primid16.8152");
#  goodrows := Filtered( [1..maxdim], 
#	                x->PositionNonZero( id[ x ] ) < maxdim+1 );
			 #while Length( goodrows ) > 0 do
  script:=CH0P_SpinUpScript( gens, 16, workid.vectors[ 1 ] );
  for i in [1..Length(workid.vectors)] do
    Print("pter = ",pter,"\n");
#    Print("Length of goodrows = ",Length(goodrows),"\n");
    # get first nonzero row vector from id
    Print("Taking row ",i,"\n");
    #v := ShallowCopy( workid.vectors[ i ] ); # dont destroy it!
    #bas := EmptySemiEchelonBasis( gens[ 1 ][ 1 ] ); #basis for subspace
    # Spin up v
    bas:=CH0P_SpinWithScript(gens, workid.vectors[i], script );
    Print("Dim of subspace = ",Length(bas),"\n");
    # copy subspace basis into big basis
    CopySubMatrix( iso*bas, basis, [1..Length(bas)], 
                   [pter..pter+Length(bas)-1],
			       [1..maxdim], [1..maxdim] );
    pter := pter + Length(bas);
    # clean id with spc and repeat
    #newgoodrows := [];
    #interbasis:=SemiEchelonRows( ExtractSubMatrix( basis,
#		 [4057..pter],[1..8152] ) );
    #for j in goodrows do
     # if not CleanRow( bas, workid[ j ], false, fail ) then
#	if j = goodrows[1] then Error("Something is terribly wrong..."); fi;
#	Add( newgoodrows, j );
 #     fi;
  #  od;
    #Print("RankMat = ",RankMat(workid),"\n");
   # goodrows := newgoodrows;
  od;
  return basis;
end;

GetMultiplicities:=function( gens, db )
  local cfstart, mults, cfend, entry, localcfend, cf, i;
  cfstart := 1;
  mults := [];
  for cf in [1..Length(db)] do
    if Dimension( db[ cf ] ) = 16 then
      Add( mults, 256 );
    else
      cfend := Length(gens[1]);
      for i in [1..Length(gens)] do
	entry := gens[ i ][ cfstart ][ cfstart ];
	localcfend := cfstart;
	while localcfend <= cfend and
	      entry = gens[ i ][ localcfend ][ localcfend ] do
	  localcfend := localcfend + 1;
	od;
	cfend := localcfend - 1;
      od;
      Add( mults, cfend - cfstart + 1 );
      cfstart := cfend + 1;
    fi;
  od;
  return mults;
end;

GetAcs:=function( mults )
  local acs, i, j;
  acs:=[];
  for i in [1..Length(mults)] do
    if mults[i] <> 256 then
      for j in [1..mults[i]] do
	Add(acs,i);
      od;
    fi;
  od;
  for j in [1..256] do
    Add(acs, 36);
  od;
  return acs;
end;
