LoadPackage("cond");
SetInfoLevel(InfoChop,2);
f := IO_File("inputfi23_1494x1494mod2.gp");
gens1 := IO_Unpickle(f);
#gens1 := List(gens1,x->TransposedMat(x^-1));   # contragredient
gens2 := IO_Unpickle(f);
#gens1 := StructuralCopy(gens2);
#     gens2 := StructuralCopy(gens1);
slpmax7 := IO_Unpickle(f);
slpsyl3 := IO_Unpickle(f);
chain := IO_Unpickle(f);
slpreps := IO_Unpickle(f);
IO_Close(f);

gens1max7 := ResultOfStraightLineProgram(slpmax7,gens1);
gens1syl3 := ResultOfStraightLineProgram(slpsyl3,gens1max7);
gens2max7 := ResultOfStraightLineProgram(slpmax7,gens2);
gens2syl3 := ResultOfStraightLineProgram(slpsyl3,gens2max7);

m := Module(gens1syl3);
n := Module(gens2syl3);
rm := Chop(m,rec(compbasis := true,ichopdepth := 20,ichoptries := 1));
rn := Chop(n,rec(compbasis := true,ichopdepth := 20,ichoptries := 1, 
                 db := rm.db));
SemiSimplicityBasis(rm,chain);
SemiSimplicityBasis(rn,chain);
CondShuffle(rm);
CondShuffle(rn);
hcinfo := PreHomcond(rm,rn);

# Condensation:
erz := [];
mtssb := List(gens1,x->rm.basis*x*rm.ibasis);
nssb := List(gens2,x->rn.basis*x*rn.ibasis);

Print("Computing reps...\n");

reps1 := ResultOfStraightLineProgram(slpreps,mtssb);
reps2 := ResultOfStraightLineProgram(slpreps,nssb);
Append(reps1,ResultOfStraightLineProgram(slpmax7,mtssb));
Append(reps2,ResultOfStraightLineProgram(slpmax7,nssb));

for i in [1..39] do
    Print(i,"\n");
    Add(erz,Homcond(reps1[i]^-1,reps2[i],hcinfo));
od;
Error();
mmm := Module(erz);
rrr := Chop(mmm);
soc := SocleSeries(mmm,rrr.db);
rad := RadicalSeries(mmm,rrr.db);
Print("\nCONDENSED TENSOR PRODUCT:\n\n");
Display(rrr); Print("\n");
Display(soc); Print("\n");
Display(rad); Print("\n\n");


