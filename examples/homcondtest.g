# testing hom cond together with basic.
  LoadPackage("basic");
  LoadPackage("cond");
  # local fix. matching.g should be included in 
  # the files read in by GAP when loading the
  # cond package. Klaus
  #
  Read(Concatenation(GAPInfo.UserHome,"/pkg/cond/gap/matching.g"));
homcondtest:=function(alg,prime,repnumber1,repnumber2)
  local gpname,rep1,res1,rep2,res2,gens1,kgens1,gens2,kgens2,
  tgens1,ktgens1,syl,chain,mt,rt,m,r,hcinfo,erz,mssb,mtssb,
  mmm,rrr,soc,rad,firstmod,f,biggens,bigmod,ch,bigsoc,
  v,vec,c,e,e1,sub,munc,muncchop,socmunc;
  #alg:=InitializeRecordAtlasGroup(gpname,prime,1);
  #AutoCalcBasic(alg);
  gpname:=alg.group;
  # get first rep.
  rep1:=AtlasGenerators(gpname,repnumber1);
  res1:=List(rep1.generators,x->CMat(x));
  res1:=condense(alg,res1,false);
  # get second rep.
  rep2:=AtlasGenerators(gpname,repnumber2);
  res2:=List(rep2.generators,x->CMat(x));
  res2:=condense(alg,res2,false);
  # Preparations:
  gens1 := res1.condgens;
  kgens1 := res1.kdgens;
  gens2 := res2.condgens;
  kgens2 := res2.kdgens;
  #
  # it is not exactly clear to  me why we need 
  # the dual here. but watch we are computing
  # Hom(V^*,V) which is isom. to V^*^* tensor V
  # which is isom. to V tensor V.
  # So in the general case we have
  # Hom(V1^*,V2) which is isom. to V1 tensor V2.
  #
  tgens1 := List(gens1,x->TransposedMat(x^-1));
  ktgens1 := List(kgens1,x->TransposedMat(x^-1));
  # just make the cond subgroup in the perm. rep
  # in order to compute a chain of subgroups.
  #
  syl := GroupWithGenerators(condense(alg,GeneratorsOfGroup(
          alg.gp),false).kdgens);
          # compute the chain
  chain := PrepareCompositionSeriesChain(syl);
  # chop both modules to get the simple modules.
  mt := Module(ktgens1);
  rt := Chop(mt,rec(compbasis := true));
  m := Module(kgens2);
  r := Chop(m,rec(compbasis := true,db := rt.db));
  # use Maschke to get the semisimple basis
  SemiSimplicityBasis(rt,chain);
  SemiSimplicityBasis(r,chain);
  # rearrange the bases, don't  know what it is doing exactly
  #
  CondShuffle(r);
  CondShuffle(rt);
  hcinfo := PreHomcond(rt,r);
  # Condensation:
  erz := [];
  # get the reps for the second factor
  # and the contragredient of the first
  # with respect to the bases computed
  # in PreHomcond (acutally as a side effect.
  #
  mssb := List(gens2,x->r.basis*x*r.ibasis);
  mtssb := List(tgens1,x->rt.basis*x*rt.ibasis);
  # condense, notice we have to take the inverses in the left
  # factor. which is a little bit odd.
  # I don't exactly why.
  # may be because we want the tensor product
  # and not the hom space?
  # I just follow the original script by Felix
  # and Max condm12dim5f3.g
  # I think the reason is to get
  # an action on homs we have to use the inverse
  # action on the domain, which is the contragredient
  # of V1, i.e. V1^*.
  #
  erz:=List([1..Length(mssb)], x->Homcond(mtssb[x]^-1,mssb[x],hcinfo));
  #erz:=List([1..Length(mssb)], x->Homcond(mtssb[x]^-1,mssb[x],hcinfo));
  #w := [1];; Add(erz,Homcond(Product(mtssb{w})^-1,Product(mssb{w}),hcinfo));
  mmm := Module(erz);
  if Length(RepresentingMatrices(alg.db[1])) <> Length(erz) then
    Error("basic has added generators");
  fi;
  rrr := Chop(mmm,rec(db:=alg.db));
  soc := SocleSeries(mmm,alg.db);
  rad := RadicalSeries(mmm,alg.db);
  Print("\nCONDENSED TENSOR PRODUCT:\n\n");
  Display(rrr); Print("\n");
  Display(soc); Print("\n");
  Display(rad); Print("\n\n");
  firstmod:=Module(gens1);
  f:=Chop(firstmod);
  # mtssb is the contragredient of the first
  # rep, but I think it should be the original
  # so replace it by transposed inverse.
  biggens:=List([1..Length(mssb)],
            x->KroneckerProduct(TransposedMat(mtssb[x]^-1),
               mssb[x]));;
  bigmod:=Module(biggens);
  ch:=Chop(bigmod,rec(db:=f.db));
  bigsoc:=SocleSeries(bigmod,ch.db);
  #
  ## uncondense have to use HomUncond
  #
  # the next entries in hcinfo seem to be missing,
  # so we put them in by hand.
  # we are computing Hom(mt,m)
  # so 
  hcinfo.Mdim:=Dimension(mt);
  hcinfo.Ndim:=Dimension(m);
  # take a vector in the socle
  # this will be a matrix
  v:=HomUncond(soc.basis[1],hcinfo);
  # make into a flat vector.
  vec:=CH0P_Unfurl(v);
  # set up an empty basis for the subspace basis
  c:=CMat([vec],vec);
  e:=EmptySemiEchelonBasis(c);
  #
  # check that is works with ordinary spin
  # in the Kronecker Product rep.
  CH0P_Spin(biggens,e,Dimension(m)*Dimension(mt),vec);
  Error("test did CH0P_Spin");
  # now do it with tensor spin
  vec:=CH0P_Unfurl(v);
  c:=CMat([vec],vec);
  e1:=EmptySemiEchelonBasis(c);
  
  # spin the vector. NOTICE: we need the inverses in the left factor.
  # compare with Homcond above. there we too had to take the inverses.
  # may that is why we have to do it here.
  # same reason applies: we are acting on Hom,
  # so we need the inverse action on the domain
  # to get an action.
  CH0P_TensorSpin(vec,List(mtssb,x->x^-1),mssb,e1,Dimension(m)*Dimension(mt));
  Error("done tensor spin");
  sub:=CH0P_TensorActionOnSubspace(List(mtssb,x->x^-1),
        mssb,e1);
  Error("done getting the matrix rep");
  munc:=Module(sub);
  muncchop:=Chop(munc);
  socmunc:=SocleSeries(munc,ch.db);
  Display(socmunc);
end;
