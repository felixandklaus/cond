LoadPackage("cond");
f := IO_File("inputfi22_78x429mod7.gp");
gens1 := IO_Unpickle(f);
#gens1 := List(gens1,x->TransposedMat(x^-1));   # contragredient
gens2 := IO_Unpickle(f);
#gens1 := StructuralCopy(gens2);
#     gens2 := StructuralCopy(gens1);
slpmax12 := IO_Unpickle(f);
slpsyl3 := IO_Unpickle(f);
chain := IO_Unpickle(f);
IO_Close(f);

gens1max12 := ResultOfStraightLineProgram(slpmax12,gens1);
gens1syl3 := ResultOfStraightLineProgram(slpsyl3,gens1max12);
gens2max12 := ResultOfStraightLineProgram(slpmax12,gens2);
gens2syl3 := ResultOfStraightLineProgram(slpsyl3,gens2max12);

m := Module(gens1syl3);
n := Module(gens2syl3);
rm := Chop(m,rec(compbasis := true));
rn := Chop(n,rec(compbasis := true,db := rm.db));
SemiSimplicityBasis(rm,chain);
SemiSimplicityBasis(rn,chain);
CondShuffle(rm);
CondShuffle(rn);
hcinfo := PreHomcond(rm,rn);

# Condensation:
erz := [];
mtssb := List(gens1,x->rm.basis*x*rm.ibasis);
nssb := List(gens2,x->rn.basis*x*rn.ibasis);

w := [1]; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo));
w := [2]; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo));
w := [1,2]; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo));
w := [2,1]; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo));
w := [1,2,1]; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo));
mmm := Module(erz);
rrr := Chop(mmm);
soc := SocleSeries(mmm,rrr.db);
rad := RadicalSeries(mmm,rrr.db);
Print("\nCONDENSED TENSOR PRODUCT:\n\n");
Display(rrr); Print("\n");
Display(soc); Print("\n");
Display(rad); Print("\n\n");


