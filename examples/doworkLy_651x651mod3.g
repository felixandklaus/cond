LoadPackage("cond");
f := IO_File("inputLy_651x651mod3.gp");
gens1 := IO_Unpickle(f);
#gens1 := List(gens1,x->TransposedMat(x^-1));   # contragredient
gens2 := IO_Unpickle(f);
#gens1 := StructuralCopy(gens2);
#     gens2 := StructuralCopy(gens1);
slpmax5 := IO_Unpickle(f);
slpk := IO_Unpickle(f);
chain := IO_Unpickle(f);
IO_Close(f);

gens1max5 := ResultOfStraightLineProgram(slpmax5,gens1);
gens1k := ResultOfStraightLineProgram(slpk,gens1max5);
gens2max5 := ResultOfStraightLineProgram(slpmax5,gens2);
gens2k := ResultOfStraightLineProgram(slpk,gens2max5);

m := Module(gens1k);
n := Module(gens2k);
rm := Chop(m,rec(compbasis := true,ichopdepth := 3,ichoptries := 2));
rn := Chop(n,rec(compbasis := true,db := rm.db));
SemiSimplicityBasis(rm,chain);
SemiSimplicityBasis(rn,chain);
CondShuffle(rm);
CondShuffle(rn);
hcinfo := PreHomcond(rm,rn);

# Condensation:
erz := [];
mtssb := List(gens1,x->rm.basis*x*rm.ibasis);
nssb := List(gens2,x->rn.basis*x*rn.ibasis);

w := [1];; Add(erz,Homcond(Product(mtssb{w})^-1, Product(nssb{w}),hcinfo));
w := [2];; Add(erz,Homcond(Product(mtssb{w})^-1, Product(nssb{w}),hcinfo));
w := [1,2];; Add(erz,Homcond(Product(mtssb{w})^-1, Product(nssb{w}),hcinfo));
w := [2,1];; Add(erz,Homcond(Product(mtssb{w})^-1, Product(nssb{w}),hcinfo));
w := [1,2,1];; Add(erz,Homcond(Product(mtssb{w})^-1, Product(nssb{w}),hcinfo));

mmm := Module(erz);
rrr := Chop(mmm);
soc := SocleSeries(mmm,rrr.db);
rad := RadicalSeries(mmm,rrr.db);
Print("\nCONDENSED TENSOR PRODUCT:\n\n");
Display(rrr); Print("\n");
Display(soc); Print("\n");
Display(rad); Print("\n\n");


