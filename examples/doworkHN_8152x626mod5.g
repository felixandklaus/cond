LoadPackage("cond");
SetInfoLevel(InfoChop,3);
f := IO_File("inputHN_8152x626mod5.gp");
gens1 := IO_Unpickle(f);
#gens1 := List(gens1,x->TransposedMat(x^-1));   # contragredient
gens2 := IO_Unpickle(f);
slpmax4 := IO_Unpickle(f);
slpk := IO_Unpickle(f);
chain := IO_Unpickle(f);
IO_Close(f);
SlotUsagePattern(slpmax4);;
SlotUsagePattern(slpk);;

#gens1max4 := ResultOfStraightLineProgram(slpmax4,gens1);
#gens1k := ResultOfStraightLineProgram(slpk,gens1max4);
#gens2max4 := ResultOfStraightLineProgram(slpmax4,gens2);
#gens2k := ResultOfStraightLineProgram(slpk,gens2max4);
f := IO_File("inputHN_8152x626mod5_2.gp");
gens1max4 := IO_Unpickle(f);
gens1k := IO_Unpickle(f);
gens2max4 := IO_Unpickle(f);
gens2k := IO_Unpickle(f);
IO_Close(f);

m := Module(gens1k);
n := Module(gens2k);
rm := Chop(m,rec(compbasis := true,ichopdepth := 10,ichoptries := 1));
rn := Chop(n,rec(compbasis := true,db := rm.db));
SemiSimplicityBasis(rm,chain);
SemiSimplicityBasis(rn,chain);
CondShuffle(rm);
CondShuffle(rn);
hcinfo := PreHomcond(rm,rn);
Print("Zeit fuer PreHomcond: ",time,"\n");

# Condensation:
erz := [];
mtssb := List(gens1,x->rm.basis*x*rm.ibasis);
nssb := List(gens2,x->rn.basis*x*rn.ibasis);

w := [1];; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo)); Print("Zeit fuer Homcond: ",time,"\n");
w := [2];; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo)); Print("Zeit fuer Homcond: ",time,"\n");
w := [1,2];; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo)); Print("Zeit fuer Homcond: ",time,"\n");
w := [2,1];; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo)); Print("Zeit fuer Homcond: ",time,"\n");
w := [1,2,1];; x := [Product(mtssb{w})^-1, Product(nssb{w})];
Add(erz,Homcond(x[1],x[2],hcinfo)); Print("Zeit fuer Homcond: ",time,"\n");
mmm := Module(erz);
rrr := Chop(mmm);
soc := SocleSeries(mmm,rrr.db);
rad := RadicalSeries(mmm,rrr.db);
Print("\nCONDENSED TENSOR PRODUCT:\n\n");
Display(rrr); Print("\n");
Display(soc); Print("\n");
Display(rad); Print("\n\n");


