LoadPackage("chop");
rep:=List(AtlasGenerators("Fi23",5).generators,CMat);
Read("/users/neunhoef/fi23/fi23slps.g");
ngens:=ResultOfStraightLineProgram(fi23_m7,rep);
kgens:=ResultOfStraightLineProgram(m7_k,ngens);
m_k:=Module(kgens);
m_k_ch:=Chop(m_k,rec(compbasis:=true,ichopdepth:=2, ichoptries:=3 ));
prep:=AtlasGenerators("Fi23",1).generators;;
nprep:=ResultOfStraightLineProgram(fi23_m7,prep);;
kprep:=ResultOfStraightLineProgram(m7_k,nprep);;
hom:=SmallerDegreePermutationRepresentation(Group(kprep));;
smallkprep:=List(kprep,x->Image(hom,x));;
chain:=PrepareCompositionSeriesChain(Group(smallkprep));
SemiSimplicityBasis(m_k_ch,chain);;
LoadPackage("cond");
CondShuffle(m_k_ch);
info:=PreHomcond(m_k_ch,m_k_ch);;
gensinssb:=List(rep, x->m_k_ch.basis*x*m_k_ch.ibasis);
x := gensinssb[1]^-1; y := gensinssb[1];

res := Homcond(x, y, info); time;
res2 := HomcondComp(x, y, info); time;

# Zeit fuer ein HomcondComp 19940 x 19940:
#  60767550 ms (rund 16 Stunden)
#
# Korrektes Ergebnis: siehe korrekt.gp in diesem Verzeichnis.
#
# Zeiten (auf schur mit -m 4000m):
#
# Urspruenglich: 51220, 48220, 49840m

Error("do not read the rest of the file");

debug := function(a,b)
local x,y;
x := CVEC_PROD_CMAT_CMAT_DISPATCH(a,b);
y := CVEC_PROD_CMAT_CMAT_BIG(a,b);
if x <> y then Error(); fi;
return x;
end;
InstallOtherMethod(\*,  [IsCMatRep and IsMatrix, IsCMatRep and IsMatrix],
1,debug);

# Timings fuer Hom(19940,19940):
#   PreHomCond: 157750 ms
#   HomCondComp:
