LoadPackage("cond");

# Preparations:
pgens := AtlasGenerators("M12",1).generators;
p := Group(pgens);
syl := SylowSubgroup(p,3);
r := FindShortGeneratorsOfSubgroup(p,syl,\in);
slpK := r.slp;
b := Basis(VectorSpace(GF(2),Elements(GF(4))));
gens := AtlasGenerators("M12",15).generators;
gens := List(gens,x->BlownUpMatrix(b,x));
List(gens,ConvertToMatrixRep);
gens := List(gens,CMat);
kgens := ResultOfStraightLineProgram(slpK,gens);
tgens := List(gens,x->TransposedMat(x^-1));
ktgens := ResultOfStraightLineProgram(slpK,tgens);
syl := GroupWithGenerators(ResultOfStraightLineProgram(slpK,pgens));
chain := PrepareCompositionSeriesChain(syl);
mt := Module(ktgens);
rt := Chop(mt,rec(compbasis := true));
m := Module(kgens);
r := Chop(m,rec(compbasis := true,db := rt.db));
SemiSimplicityBasis(rt,chain);
SemiSimplicityBasis(r,chain);
CondShuffle(r);
CondShuffle(rt);
hcinfo := PreHomcond(rt,r);

# Condensation:
erz := [];
mssb := List(gens,x->r.basis*x*r.ibasis);
mtssb := List(tgens,x->rt.basis*x*rt.ibasis);
w := [1];; Add(erz,Homcond(Product(mtssb{w})^-1,Product(mssb{w}),hcinfo));
w := [2];; Add(erz,Homcond(Product(mtssb{w})^-1,Product(mssb{w}),hcinfo));
w := [1,2];; Add(erz,Homcond(Product(mtssb{w})^-1,Product(mssb{w}),hcinfo));
w := [2,1];; Add(erz,Homcond(Product(mtssb{w})^-1,Product(mssb{w}),hcinfo));
w := [1,2,1];; Add(erz,Homcond(Product(mtssb{w})^-1,Product(mssb{w}),hcinfo));
mmm := Module(erz);
rrr := Chop(mmm);
soc := SocleSeries(mmm,rrr.db);
rad := RadicalSeries(mmm,rrr.db);
Print("\nCONDENSED TENSOR PRODUCT:\n\n");
Display(rrr); Print("\n");
Display(soc); Print("\n");
Display(rad); Print("\n\n");

# The tensor product itself:
big := List([1..Length(gens)],i->KroneckerProduct(gens[i],gens[i]));
mbig := Module(big);
rbig := Chop(mbig);
soc := SocleSeries(mbig,rbig.db);
rad := RadicalSeries(mbig,rbig.db);
Print("TENSOR PRODUCT:\n\n");
Display(rbig); Print("\n");
Display(soc); Print("\n");
Display(rad); Print("\n");

# Now find out which modules condense to zero:
rguck := false;   # to make this global variable bound
for i in [1..Length(rbig.db)] do
    # Restrict to K:
    guck := ResultOfStraightLineProgram(slpK,RepresentingMatrices(rbig.db[i]));
    mguck := Module(guck);
    rguck := Chop(mguck);
    nrtriv := First([1..Length(rguck.db)],j->IsTrivialModule(rguck.db[j]));
    if nrtriv = fail then
        Print("#",i," condenses to 0.\n");
    else
        isotypic := IsotypicComponentSocle(rguck.db[nrtriv],mguck);
        if Length(isotypic.cfposs) = 0 then
            Print("#",i," condenses to 0.\n");
        else
            Print("#",i," condenses to dim ",Length(isotypic.cfposs),"\n");
        fi;
    fi;
od;
